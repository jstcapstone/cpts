﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CPTS.Startup))]
namespace CPTS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
