﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CPTS.Repository.Models;

namespace CPTS.Models
{
    public class AddContactsActivity
    {
        public Activity Activity { get; set; }

        public string ContactEmail { get; set; }

        public string PartnerName { get; set; }

        public List<string> Contacts { get; set; }
    }
}