﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Repository.Models;

namespace CPTS.Models
{
    public class CreateCampusUnitData
    {
        public CampusUnitData CampusUnitData { get; set; }

        public string CampusUnitName { get; set; } 
        
        public List<string> Semesters { get; set; }    
        
        public List<string> CampusUnits { get; set; }  
    }
}