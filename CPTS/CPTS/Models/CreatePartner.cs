﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Repository.Models;

namespace CPTS.Models
{
    public class CreatePartner
    {
        public Partner Partner { get; set; }

        public Contact Contact { get; set; }

        public List<string> CampusUnitNames { get; set; }

        public string CampusUnitName { get; set; }

        public int[] selectedTags { get; set; }

        public IEnumerable<SelectListItem> Tags { get; set; }
    }
}