﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CPTS.Repository.Models;

namespace CPTS.Models
{
    public class CreateActivity
    {
        public Activity Activity { get; set; }

        public List<string> CampusUnitNames { get; set; }

        public string CampusUnitName { get; set; }
    }
}