﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CPTS.Repository.Models;


namespace CPTS.Models
{
    public class AddTagsPartner
    {
        public Partner Partner { get; set; }

        public List<string> Tags { get; set; }

        public string SelectedTag { get; set; }

        public string SearchString { get; set; }
    }
}