﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Repository.Models;

namespace CPTS.Models
{
    public class HomeModel
    {
        public Partner Partner { get; set; }

        public List<CampusUnit> CampusUnits { get; set; }

        public string SearchString { get; set; }

        public List<SelectListItem> Tags { get; set; }

        public List<SelectListItem> Units { get; set; }
    }
}