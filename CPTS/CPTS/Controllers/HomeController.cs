﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Models;
using CPTS.Repository.Context;
using CPTS.Repository.Models;
using System.Text;
using CPTS.Repository.Helpers;
using CPTS.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class HomeController : Controller
    {
        PartnerRepository partnerRepository = new PartnerRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        ContactRepository contactRepository = new ContactRepository();
        TagRepository tagRepository = new TagRepository();

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult Index(string searchPartner, string searchContact, string searchTag, string searchCampusUnit)
        {
            var homePartners = GetListOfHomeModelsBasedOnPartner(searchPartner);
            if (searchContact != "")
            {
                homePartners = GetListBasedOnContactName(searchContact, homePartners);
            }
            else
            {
                foreach (var model in homePartners) model.SearchString += ConstantsChars.CharForStringSeperator;
            }
            if (searchTag != "")
            {
                homePartners = GetListBasedOnTag(searchTag, homePartners);
            }
            else
            {
                foreach (var model in homePartners) model.SearchString += ConstantsChars.CharForStringSeperator;
            }
            if (searchCampusUnit != "")
            {
                homePartners = GetListBasedOnCampusUnit(searchCampusUnit, homePartners);
            }
            else
            {
                foreach (var model in homePartners) model.SearchString += ConstantsChars.CharForStringSeperator;
            }
          
            var nameHelper = new SafeURL();
            foreach (var homePartner in homePartners)
            {
                homePartner.Partner.URLSafeName = nameHelper.CreateURLSafeName(homePartner.Partner.PartnerName);
                
                homePartner.Partner.campusUnitString = "";
                for (int i = 0; i < homePartner.CampusUnits.Count; i++)
                {
                    homePartner.Partner.campusUnitString += homePartner.CampusUnits[i].Name;
                    if (i < homePartner.CampusUnits.Count - 1) homePartner.Partner.campusUnitString += ", ";
                }
            }

            ViewBag.Message = homePartners.Count;
            ViewBag.TagList = AddTagsToList();
            ViewBag.CampusList = AddCampusUnitList();
            return View(homePartners);
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Create()
        {
            var newPartner = new CreatePartner();
            CreatePartnerForCreate(newPartner, true);
            return View(newPartner);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Create(CreatePartner newPartner)
        {
            newPartner.Partner.PartnerName = KeyTrimmer.TrimTrailingSpaces(newPartner.Partner.PartnerName); //Remove Spaces at the end of the key

            if (CheckForRequiredFields(newPartner))
            {
                if (CheckForDuplicateKey(newPartner))
                {
                    using (var context = new CPTSContext())
                    {
                        List<Contact> contacts = new List<Contact>(); // create a list of Contacts for later
                        List<Activity> activities = new List<Activity>(); // create a list of Activities for later
                        List<Tag> addedTags = new List<Tag>(); // create a list of Tags for later
                        var activity = new Activity();
                        var date = DateTime.Now;
                        date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind); // remove milliseconds

                        activity.Partner = newPartner.Partner; // set the Activity's Partner to the one created
                        activity.CampusUnit = context.CampusUnits.Find(newPartner.CampusUnitName); // find the CampusUnit
                        activity.Subject = "First contact with " + newPartner.Partner.PartnerName + " and " +
                                           newPartner.Contact.FirstName + " " + newPartner.Contact.LastName + " on " +
                                           date; // add a default Subject
                        activity.Date = date; // add the date
                        activities.Add(activity); // add the Activity to the list of Activities

                        newPartner.Contact.Activities = activities; // add the list of Activities to the Contact's list of Activities
                        newPartner.Contact.Partner = newPartner.Partner; // set the Contact's Partner

                        contacts.Add(newPartner.Contact); // add the Contact to a list of Contacts

                        activity.Contacts = contacts; // add the list of Contacts to the Activity

                        newPartner.Partner.Contacts = contacts; // add the list of Contacts to

                        if (newPartner.selectedTags != null)
                        {
                            var tagNames = context.Tags.ToList(); // get all the tags
                            foreach (var tagId in newPartner.selectedTags)
                            {
                                if (tagId != 0) //Check that element to add is not the default/empty tag
                                    addedTags.Add(tagNames.ElementAt(tagId)); // add the chosen tags to a list
                            }
                            newPartner.Partner.SearchTags = addedTags; // set the Partner list to the one of addedTags
                        }

                        context.Partners.Add(newPartner.Partner);
                        context.Contacts.Add(newPartner.Partner.Contacts.First());
                        context.Activities.Add(newPartner.Partner.Contacts.First().Activities.First());
                        context.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                
            }
            return View(CreatePartnerForCreate(newPartner, false));
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportPartners(string id)
        {
            string csvFileString = Export(id, false, false);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportCommunityPartners.csv");
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportPartnersContacts(string id)
        {
            string csvFileString = Export(id, true, false);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportPartnersContacts.csv");
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportPartnersContactsCampusUnitData(string id)
        {
            string csvFileString = Export(id);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportPartnersContactsCampusUnitData.csv");
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportSpecificCampusUnitData(string id)
        {
            string csvFileString = Export(id, true, true, true);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExportSpecificCampusUnitDataOnly.csv");
        }

        public string Export(string searchString, bool contacts = true, bool campusUnitData = true, bool specificCampusUnitData = false)
        {
            ExcelExport export = new ExcelExport();
            ContactRepository contactRepository = new ContactRepository();
            TagRepository tagRepository = new TagRepository();

            string csvFileString = "No search results to export.";

            if (searchString != null)
            {
                var searchArray = searchString.Split(ConstantsChars.CharForStringSeperator);

                List<HomeModel> homePartners = GetListOfHomeModelsBasedOnPartner(searchArray[0]);
                if (searchArray[1] != "")
                {
                    homePartners = GetListBasedOnContactName(searchArray[1], homePartners);
                }
                if (searchArray[2] != "")
                {
                    homePartners = GetListBasedOnTag(searchArray[2], homePartners);
                }
                if (searchArray[3] != "")
                {
                    homePartners = GetListBasedOnCampusUnit(searchArray[3], homePartners);
                }
                homePartners = homePartners.OrderBy(p => p.Partner.PartnerName).ToList();

                if (!contacts && !campusUnitData) csvFileString = export.ExportPartners(homePartners);
                else if (contacts && !campusUnitData && !specificCampusUnitData) csvFileString = export.ExportPartnersContacts(homePartners);
                else if (contacts && campusUnitData && !specificCampusUnitData) csvFileString = export.ExportPartnersContactsCampusUnitData(homePartners);
                else
                {
                    if (searchArray[3] != "") csvFileString = export.ExportPartnersContactsCampusUnitData(homePartners, searchArray[3]);
                    else csvFileString = "Include campus unit in search.";
                }
            }

            return csvFileString;
        }

        public List<HomeModel> GetListOfHomeModelsBasedOnPartner(string searchPartner)  
        {
            var homeModels = new List<HomeModel>();
            var partners = partnerRepository.GetPartnersByName(searchPartner);
            foreach (var partner in partners)
            {
                var homeModel = new HomeModel();
                homeModel.Partner = partner;
                homeModel.CampusUnits = campusUnitRepository.FindCampusUnitsForPartner(partner);
                homeModels.Add(homeModel);
                homeModel.SearchString = searchPartner;               
            }

            return homeModels;
        }

        public List<HomeModel> GetListBasedOnContactName(string contactName, List<HomeModel> partnerHomeModels) 
        {
            var contacts = contactRepository.FindContactsBasedName(contactName);
            var homeModels = new List<HomeModel>();
            foreach (var contact in contacts)
            {
                var partner = partnerRepository.GetPartnerByContactEmail(contact.Email);
                if (partnerHomeModels.Any(h => h.Partner.PartnerName == partner.PartnerName))
                {
                    var contactPartner = partnerHomeModels.First(p => p.Partner.PartnerName == partner.PartnerName);
                    contactPartner.SearchString += ConstantsChars.CharForStringSeperator + contactName;
                    homeModels.Add(contactPartner);
                }
            }
            return homeModels;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult CreateTag()
        {
            var tag = new Tag();
            return View(tag);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult CreateTag(Tag newTag)
        {
            if (tagRepository.GetTagByName(newTag.TagName) == null)
            {
                tagRepository.Add(newTag);
                tagRepository.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Message = "Tag already exists!";
            return View(newTag);
        }

        public ActionResult About()
        {
            return View();
        }

        private List<HomeModel> GetListBasedOnTag(string searchTag, List<HomeModel> partnerHomeModels)
        {
            var homeModels = new List<HomeModel>();
            var partners = partnerRepository.GetByTag(searchTag);
            foreach (var partner in partners)
            {
                if (partnerHomeModels.Any(h => h.Partner.PartnerName == partner.PartnerName))
                {
                    var homeModel = partnerHomeModels.First(p => p.Partner.PartnerName == partner.PartnerName);
                    homeModel.SearchString += ConstantsChars.CharForStringSeperator + searchTag;
                    homeModels.Add(homeModel);
                }
            }

            return homeModels;
        }

        private List<HomeModel> GetListBasedOnCampusUnit(string searchCampusUnit, List<HomeModel> partnerHomeModels)
        {
            var homeModels = new List<HomeModel>();
            var partners = partnerRepository.GetByCampusUnit(searchCampusUnit);
            foreach (var partner in partners)
            {
                if (partnerHomeModels.Any(h => h.Partner.PartnerName == partner.PartnerName))
                {
                    var homeModel = partnerHomeModels.First(p => p.Partner.PartnerName == partner.PartnerName);
                    homeModel.SearchString += ConstantsChars.CharForStringSeperator + searchCampusUnit;
                    homeModels.Add(homeModel);
                }
            }

            return homeModels;
        }

        private bool CheckForRequiredFields(CreatePartner partner)
        {
            if (partner.Partner.PartnerName != null && partner.Contact.Email != null && partner.CampusUnitName != null)
            {
                return true;
            }
            ViewBag.Message = "You are missing required field(s)!";
            return false;
        }

        private CreatePartner CreatePartnerForCreate(CreatePartner partner, bool newCreatePartner)
        {
            List<string> tagNames = tagRepository.GetAllTagNames();
            if (newCreatePartner)
            {
                partner.selectedTags = new int[tagNames.Count];
            }

            SelectListItem[] tagListCreation = new SelectListItem[tagNames.Count];
            for (int i = 0; i < tagNames.Count; i++)
            {
                tagListCreation[i] = new SelectListItem { Value = "" + i, Text = "" + tagNames.ElementAt(i) };
            }

            partner.Tags = tagListCreation;

            partner.CampusUnitNames = campusUnitRepository.GetAllCampusUnitsNames();
            return partner;
        }

        private bool CheckForDuplicateKey(CreatePartner createPartner)
        {
            bool notDuplicate = true;
            if (partnerRepository.GetSinglePartnerByName(createPartner.Partner.PartnerName) != null)
            {
                ViewBag.Message = "That partner name already exists in the system.";
                notDuplicate = false;
            }
            else if (contactRepository.GetSingleContactByEmail(createPartner.Contact.Email) != null)
            {
                ViewBag.Message = "That contact email already exists in the system.";
                notDuplicate = false;
            }
            return notDuplicate;
        }

        private List<SelectListItem> AddTagsToList()
        {
            var tags = new List<SelectListItem>();
            var tagNames = tagRepository.GetAllTagNames();
            foreach (var tagName in tagNames)
            {
                var tag = new SelectListItem {Text = tagName, Value = tagName};
                tags.Add(tag);
            }
            return tags;
        }

        private List<SelectListItem> AddCampusUnitList()
        {
            var units = new List<SelectListItem>();
            var campusNames = campusUnitRepository.GetAllCampusUnitsNames();
            var blankUnit = new SelectListItem {Text = "", Value = ""};
            units.Add(blankUnit);
            foreach (var campusName in campusNames)
            {
                var unit = new SelectListItem {Text = campusName, Value = campusName};
                units.Add(unit);
            }
            return units;
        }
    }
}