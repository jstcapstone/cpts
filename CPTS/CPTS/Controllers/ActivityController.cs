﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Helpers;
using CPTS.Models;
using CPTS.Repository.Context;
using CPTS.Repository.Models;
using CPTS.Repository.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class ActivityController : Controller
    {

        ActivityRepository activityRepository = new ActivityRepository();
        PartnerRepository partnerRepository = new PartnerRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        ContactRepository contactRepository = new ContactRepository();
        SafeURL URLHelper = new SafeURL();
        // GET: Activity
        public ActionResult Details(string id, DateTime date, string partnerName = "", string email = "")
        {
            var activity = activityRepository.FindActivityUsingSubjectContactDateAndPartner(id, date, URLHelper.CreateNameFromSafeName(partnerName), URLHelper.CreateEmailFromSafeEmail(email));
            activity.Partner = partnerRepository.GetPartnerByActivity(activity);
            activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivity(activity);
            activity.Contacts = contactRepository.FindContactsByActivity(activity);
            foreach (var contact in activity.Contacts) contact.URLSafeEmail = URLHelper.CreateURLSafeEmail(contact.Email);
            activity.ContactEmail = URLHelper.CreateURLSafeEmail(activity.Contacts.FirstOrDefault().Email);
            activity.URLSafeSubject = id;
            ViewBag.ContactCount = activity.Contacts.Count;

            return View(activity);
        }


        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult AddContacts(string id, string email)
        {
            var activity = activityRepository.FindSingleActivityFromSubject(id);
            activity.URLSafeSubject = id;
            var editActivity = new AddContactsActivity {Activity = activity};
            var partner = partnerRepository.GetPartnerByActivity(activity);
            editActivity.PartnerName = partner.PartnerName;
            editActivity.Activity.Partner = partner;
            editActivity.Contacts = contactRepository.ReturnAllContactsEmails();
            var contacts = contactRepository.FindContactsByActivity(activity);
            foreach (var contact in contacts)
            {
                editActivity.Contacts.Remove(contact.Email); // remove the Activity's Contacts from the list  
            }
            editActivity.ContactEmail = email;
            return View(editActivity);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult AddContacts(AddContactsActivity editActivity)
        {
            using (var context = new CPTSContext())
            {
                string subject = editActivity.Activity.URLSafeSubject.Replace(ConstantsChars.ReplacementCharForForwardSlash, '/');
                subject = subject.Replace(ConstantsChars.ReplacementCharForColon, ':');
                subject = subject.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
                var updatedActivity = context.Activities.First(a => a.Subject == subject && a.Partner.PartnerName == editActivity.PartnerName && a.Date == editActivity.Activity.Date); // find the Activity
                updatedActivity.Partner = context.Partners.Find(editActivity.PartnerName); // find the Partner
                updatedActivity.Contacts = new List<Contact> { context.Contacts.Find(editActivity.ContactEmail) }; // create a list of Contacts with the new Contact
                updatedActivity.CampusUnit =
                    context.CampusUnits.First(c => c.Activities.Any(a => a.Subject == updatedActivity.Subject
                                                                         &&
                                                                         DbFunctions.TruncateTime(a.Date) ==
                                                                         DbFunctions.TruncateTime(updatedActivity.Date)
                                                                         &&
                                                                         a.Partner.PartnerName ==
                                                                         updatedActivity.Partner.PartnerName)); // find the CampusUnit
                var editContact = context.Contacts.Find(editActivity.ContactEmail); // find the new Contact to be added to the Activity
                context.Entry(editContact).State = EntityState.Modified; // Update the Contact
                context.SaveChanges();
                editActivity.ContactEmail = editActivity.ContactEmail.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            }
            return RedirectToAction("Details", new { id = editActivity.Activity.URLSafeSubject, date = editActivity.Activity.Date, email = editActivity.ContactEmail });
        }


        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult ActivityEdit(string id, string email)
        {
            var activity = activityRepository.FindSingleActivityFromSubject(id);
            activity.Partner = partnerRepository.GetPartnerByActivity(activity);
            activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivity(activity);
            activity.Contacts = contactRepository.FindContactsByActivity(activity);
            activity.ContactEmail = email;
            activity.URLSafeSubject = id;

            if (activity.Notes == null)
            {
                activity.Notes = "";
            }

            return View(activity);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult ActivityEdit(Activity editActivity)//, string Subject)
        {
            editActivity.URLSafeSubject = editActivity.Subject.Replace('/', ConstantsChars.ReplacementCharForForwardSlash);     //Construct URL Safe Email
            editActivity.URLSafeSubject = editActivity.URLSafeSubject.Replace(':', ConstantsChars.ReplacementCharForColon);
            editActivity.URLSafeSubject = editActivity.URLSafeSubject.Replace('.', ConstantsChars.ReplacementCharForPeriod);

            editActivity.Date = activityRepository.GetDateByActivitySubject(editActivity.URLSafeSubject);
            editActivity.Partner = partnerRepository.GetPartnerByActivity(editActivity);
            editActivity.Partner = partnerRepository.GetSinglePartnerByName(editActivity.Partner.PartnerName);
            editActivity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivity(editActivity);
            editActivity.Contacts = contactRepository.FindContactsByActivity(editActivity);

            //The hiccup comes when the validation is called and it doesn't appreciate the partner field
            //if (ModelState.IsValid)
            //{
                using (var context = new CPTSContext())
                {
                    context.Entry(editActivity).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return RedirectToAction("Details", new { id = editActivity.Subject, date = editActivity.Date, email = editActivity.ContactEmail });
            //}
            //return View(editActivity);
        }
    }
}