﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Models;
using CPTS.Repository.Context;
using CPTS.Repository.Models;
using System.Text;
using CPTS.Repository.Helpers;
using CPTS.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class PartnerController : Controller
    {
        ContactRepository contactRepository = new ContactRepository();
        PartnerRepository partnerRepository = new PartnerRepository();
        TagRepository tagRepository = new TagRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        ActivityRepository activityRepository = new ActivityRepository();
        SafeURL nameHelper = new SafeURL();

        // GET: Partner
        public ActionResult Details(string id, string search)
        {
            var partner = partnerRepository.GetSinglePartnerByName(id);
            partner.URLSafeName = id;
            var contacts = contactRepository.FindContactsFromPartnerName(partner.PartnerName);
            partner.SearchTags = tagRepository.FindTagsFromPartnerName(partner.PartnerName);
            partner.Contacts = contacts;
            partner.Activities = activityRepository.GetAllActivitiesForAPartner(partner.PartnerName);
            partner.tagString = "";

            for(int i = 0; i < partner.SearchTags.Count; i++)
            {
                partner.tagString += partner.SearchTags[i].TagName;
                if (i < partner.SearchTags.Count - 1) partner.tagString += ", ";
            }

            List<CampusUnit> campusUnits = campusUnitRepository.FindCampusUnitsForPartner(partner);
            for (int i = 0; i < campusUnits.Count; i++)
            {
                partner.campusUnitString += campusUnits[i].Name;
                if (i < campusUnits.Count - 1) partner.campusUnitString += ", ";
            }

            foreach (var partnerActivity in partner.Activities)
            {
                partnerActivity.Partner = partner;
                partnerActivity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivityNonTruncateTime(partnerActivity);
                partnerActivity.URLSafeSubject = nameHelper.CreateUrlSafeSubject(partnerActivity.Subject);
            }

            if (search == null) partner.SearchString = "" + ConstantsChars.CharForStringSeperator + ConstantsChars.CharForStringSeperator + ConstantsChars.CharForStringSeperator;
            else partner.SearchString = search;
            ViewBag.Message = partner.Contacts.Count;

            return View(partner);
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateContact(string id, string search)
        {
            var contact = new Contact();
            contact.Partner = partnerRepository.GetSinglePartnerByName(id);
            contact.Partner.SearchString = search;
            contact.Partner.URLSafeName = id;
            return View(contact);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateContact(Contact newContact, string search)
        {
            newContact.Email = KeyTrimmer.TrimTrailingSpaces(newContact.Email);     //Trim white spaces off of contact email
            newContact.URLSafeEmail = KeyTrimmer.TrimTrailingSpaces(newContact.URLSafeEmail);
            newContact.Partner = partnerRepository.GetSinglePartnerByName(newContact.Partner.PartnerName);
            newContact.Partner.URLSafeName = nameHelper.CreateURLSafeName(newContact.Partner.PartnerName);
            newContact.Partner.SearchString = search;

            if (CheckForRequiredFields(newContact))
            {
                if (CheckForDuplicateKeys(newContact))
                {
                    var contactList = new List<Contact> { newContact };
                    newContact.Partner.Contacts = contactList;
                    partnerRepository.Update(newContact.Partner);
                    partnerRepository.SaveChanges();
                    return RedirectToAction("Details", new { id = nameHelper.CreateURLSafeName(newContact.Partner.PartnerName) });
                }
            }            

            return View(newContact);
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult Export(string id)
        {
            ExcelExport export = new ExcelExport();
            var homeModel = new HomeModel();
            var homeModels = new List<HomeModel>();      
            homeModel.Partner = partnerRepository.GetSinglePartnerByName(id);
            homeModel.CampusUnits = campusUnitRepository.FindCampusUnitsForPartner(homeModel.Partner);
            homeModels.Add(homeModel);

            string csvFileString = export.ExportPartnersContactsCampusUnitData(homeModels);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExport.csv");
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Edit(string id, string search)
        {
            var partner = partnerRepository.GetSinglePartnerByName(id);
            partner.SearchString = search;
            partner.URLSafeName = id;
            return View(partner);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Edit(Partner editPartner, string search)
        {
            editPartner.SearchString = search;
            if (ModelState.IsValid)
            {
                using (var context = new CPTSContext())
                {
                    context.Entry(editPartner).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return RedirectToAction("Details", new { id = nameHelper.CreateURLSafeName(editPartner.PartnerName) });
            }
            return View(editPartner);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            var partner = partnerRepository.GetSinglePartnerByName(id);
            partner.URLSafeName = id;
            return View(partner);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(string id)
        {
            var partner = partnerRepository.GetSinglePartnerByName(id);
            partner.URLSafeName = id;
            if (contactRepository.FindContactsFromPartnerName(partner.PartnerName).Count != 0)
            {
                ViewBag.Message = "Must delete all Contacts before deleting " + partner.PartnerName;
                return View(partner);
            }
            partnerRepository.Remove(partner);
            partnerRepository.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult AddTag(string id, string search)
        {
            var partner = partnerRepository.GetSinglePartnerByName(id);
            var editPartner = new AddTagsPartner { Partner = partner };
            editPartner.SearchString = search;
            editPartner.SelectedTag = "";   //Supply an empty value for selected tag name, so that the view doesn't complain
            editPartner.Tags = tagRepository.GetAllTagNames();
            var tagObjects = tagRepository.FindTagsFromPartnerName(partner.PartnerName);
            editPartner.Partner.SearchTags = tagObjects;
            List<string> tagNames = new List<string>();
            foreach(var tag in tagObjects)
            {
                tagNames.Add(tag.TagName);      //Convert collected list of tags to tagNames
            }
            foreach(var item in tagNames)
            {
                editPartner.Tags.Remove(item);  //Trim results from 'GetAllTagNames' by removing tagNames Associated with the partner
            }

            return View(editPartner);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult AddTag(AddTagsPartner addedTagPartner)
        {
            if (addedTagPartner.SelectedTag != null)
            {
                using (var context = new CPTSContext())
                {
                    var updatedPartner = context.Partners.Find(addedTagPartner.Partner.PartnerName);    //Get the partner
                    var newTag = context.Tags.Find(addedTagPartner.SelectedTag);
                    //"Rebuild" UpdatedPartner Prior to saving it
                    updatedPartner.Activities = context.Activities.Where(a => a.Partner.PartnerName == updatedPartner.PartnerName).ToList();
                    updatedPartner.Contacts = context.Contacts.Where(c => c.Partner.PartnerName == updatedPartner.PartnerName).ToList();
                    updatedPartner.SearchString = addedTagPartner.SearchString;
                    updatedPartner.SearchTags = new List<Tag>();
                    updatedPartner.URLSafeName = nameHelper.CreateURLSafeName(updatedPartner.PartnerName);
                    updatedPartner.SearchTags.Add(newTag);
                    context.Entry(updatedPartner).State = EntityState.Modified; // Update the Contact
                    context.SaveChanges();
                }
            }

            return RedirectToAction("Details", new { id = addedTagPartner.Partner.PartnerName, search = addedTagPartner.SearchString});
        }
        

        private bool CheckForRequiredFields(Contact newContact)
        {
            if (newContact.Email != null && newContact.Partner.PartnerName != null)
            {
                return true;
            }
            ViewBag.Message = "You are missing required field(s)!";
            return false;
        }

        private bool CheckForDuplicateKeys(Contact toCheck)
        {
            toCheck.URLSafeEmail = toCheck.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            if(contactRepository.GetSingleContactByEmail(toCheck.URLSafeEmail) == null)
            {
                return true;
            }
            ViewBag.Message = "A contact with that email already exists in the system.";

            return false;

        }
    }
}