﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Helpers;
using CPTS.Repository.Context;
using CPTS.Repository.Helpers;
using CPTS.Repository.Models;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class CampusUnitDataController : Controller
    {
        CampusUnitDataRepository campusUnitDataRepository = new CampusUnitDataRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        ContactRepository contactRepository = new ContactRepository();
        SafeURL safeUrlHelper = new SafeURL();

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult Details(string uwsProgram, string uwsDepartmentEvent, string uwsStaff, string semester, string email)
        {
            CampusUnitData campusUnitData = campusUnitDataRepository.GetSingleCampusUnitDataRecord(uwsProgram, uwsDepartmentEvent, uwsStaff, semester, email.Replace(ConstantsChars.ReplacementCharForPeriod, '.'));
            campusUnitData.AssignedContact = contactRepository.GetSingleContactByEmail(email);
            campusUnitData.AssignedContact.URLSafeEmail = safeUrlHelper.CreateURLSafeEmail(campusUnitData.AssignedContact.Email);
            campusUnitData.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(campusUnitData);
            return View(campusUnitData);
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult EditCampusUnitData(string uwsProgram, string uwsDepartmentEvent, string uwsStaff, string semester, string email)
        {
            CampusUnitData editCampusUnitData = campusUnitDataRepository.GetSingleCampusUnitDataRecord(uwsProgram, uwsDepartmentEvent, uwsStaff, semester, email.Replace(ConstantsChars.ReplacementCharForPeriod, '.'));
            editCampusUnitData.AssignedContact = contactRepository.GetSingleContactByEmail(email);
            editCampusUnitData.AssignedContact.URLSafeEmail = safeUrlHelper.CreateURLSafeEmail(editCampusUnitData.AssignedContact.Email);
            return View(editCampusUnitData);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult EditCampusUnitData(CampusUnitData editCampusUnitData)
        {
            editCampusUnitData.AssignedContact = contactRepository.GetSingleContactByEmail(editCampusUnitData.AssignedContact.URLSafeEmail);
            editCampusUnitData.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(editCampusUnitData);

            using (var context = new CPTSContext())
            {
                context.Entry(editCampusUnitData).State = EntityState.Modified;
                context.SaveChanges();
            }
            return RedirectToAction("Details",
                new
                {
                    uwsProgram = editCampusUnitData.UWSProgram,
                    uwsDepartmentEvent = editCampusUnitData.DepartmentEvent,
                    uwsStaff = editCampusUnitData.UWSStaff,
                    semester = editCampusUnitData.Semester,
                    email = editCampusUnitData.AssignedContact.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod)
                });
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Delete(string uwsProgram, string uwsDepartmentEvent, string uwsStaff, string semester, string email)
        {
            CampusUnitData campusUnitData = campusUnitDataRepository.GetSingleCampusUnitDataRecord(uwsProgram, uwsDepartmentEvent, uwsStaff, semester, email.Replace(ConstantsChars.ReplacementCharForPeriod, '.'));
            campusUnitData.AssignedContact = contactRepository.GetSingleContactByEmail(email);
            campusUnitData.AssignedContact.URLSafeEmail = safeUrlHelper.CreateURLSafeEmail(campusUnitData.AssignedContact.Email);
            campusUnitData.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(campusUnitData);
            return View(campusUnitData);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Delete(CampusUnitData deletedCampusUnitData)
        {
            using (var context = new CPTSContext())
            {
                var campusUnitData =
                    context.CampusUnitDataRecords.FirstOrDefault(
                        a =>
                            a.UWSProgram == deletedCampusUnitData.UWSProgram &&
                            a.UWSStaff == deletedCampusUnitData.UWSStaff && a.Semester == deletedCampusUnitData.Semester &&
                            a.DepartmentEvent == deletedCampusUnitData.DepartmentEvent && a.AssignedContact.Email == deletedCampusUnitData.AssignedContact.Email);
                context.CampusUnitDataRecords.Remove(campusUnitData);
                context.SaveChanges();
            }
            return RedirectToAction("Details", "Contact", new {id = deletedCampusUnitData.AssignedContact.URLSafeEmail});
        }
    }
}