﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Helpers;
using CPTS.Models;
using CPTS.Repository.Context;
using CPTS.Repository.Models;
using CPTS.Repository.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class ContactController : Controller
    {
        ContactRepository contactRepository = new ContactRepository();
        ActivityRepository activityRepository = new ActivityRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        CampusUnitDataRepository campusUnitDataRepository = new CampusUnitDataRepository();
        PartnerRepository partnerRepository = new PartnerRepository();
        SafeURL nameHelper = new SafeURL();

        public ActionResult Details(string id, string search)
        {
            var contact = contactRepository.GetSingleContactByEmail(id);
            contact.URLSafeEmail = id;
            contact.Activities = activityRepository.FindActivitiesFromContactEmail(contact.Email);
            contact.CampusUnitData = campusUnitDataRepository.FindCampusUnitDataFromContactEmail(contact.Email);
            contact.Partner = partnerRepository.GetPartnerByContactEmail(contact.Email);
            contact.Partner.URLSafeName = nameHelper.CreateURLSafeName(contact.Partner.PartnerName);
            if (search == null)
            {
                contact.Partner.SearchString = "" + ConstantsChars.CharForStringSeperator
                    + ConstantsChars.CharForStringSeperator + ConstantsChars.CharForStringSeperator;
            }
            else contact.Partner.SearchString = search;

            foreach (var activity in contact.Activities)    
            {
                activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivityNonTruncateTime(activity);
                activity.URLSafeSubject = nameHelper.CreateUrlSafeSubject(activity.Subject);
            }
            foreach (var campusUnitData in contact.CampusUnitData)
            {
                campusUnitData.AssignedContact = contact;
                campusUnitData.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(campusUnitData);
            }
            ViewBag.Message = contact.Activities.Count;
            return View(contact);
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateActivity(string id)
        {
            var contact = new List<Contact> {contactRepository.GetSingleContactByEmail(id)};
            contact.First().URLSafeEmail = contact.First().Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            var createActivity = new CreateActivity();
            var activity = new Activity();
            activity.ContactEmail = id.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            activity.Contacts = contact;
            var date = DateTime.Now;
            activity.Date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind); // remove milliseconds
            createActivity.Activity = activity;
            createActivity.CampusUnitNames = campusUnitRepository.GetAllCampusUnitsNames();
            return View(createActivity);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateActivity(CreateActivity newActivity)
        {
            newActivity.Activity.Subject = KeyTrimmer.TrimTrailingSpaces(newActivity.Activity.Subject); //These two line remove any trailing whitespace from subject
            newActivity.Activity.URLSafeSubject = KeyTrimmer.TrimTrailingSpaces(newActivity.Activity.URLSafeSubject);

            if (CheckForRequiredFields(newActivity))
            {
                if (CheckForDuplicateKeysActivityCreate(newActivity))
                {
                    using (var context = new CPTSContext())
                    {
                        newActivity.Activity.Partner =
                            context.Partners.FirstOrDefault(
                                p => p.Contacts.Any(c => c.Email == newActivity.Activity.ContactEmail));
                        // find the Partner of the Contact and add it

                        newActivity.Activity.Contacts = new List<Contact>
                        {
                            context.Contacts.Find(newActivity.Activity.ContactEmail)
                        };
                        // find the Contact and make a list and add it

                        newActivity.Activity.CampusUnit = context.CampusUnits.Find(newActivity.CampusUnitName);

                        context.Activities.Add(newActivity.Activity);
                        context.SaveChanges();
                    }
                    return RedirectToAction("Details", new { id = newActivity.Activity.ContactEmail.Replace('.', ConstantsChars.ReplacementCharForPeriod) });
                }
            }

            newActivity.CampusUnitNames = campusUnitRepository.GetAllCampusUnitsNames();
            newActivity.Activity.Contacts = new List<Contact> { contactRepository.GetSingleContactByEmail(newActivity.Activity.ContactEmail) };
            newActivity.Activity.Contacts.First().URLSafeEmail = newActivity.Activity.Contacts.First().Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            return View(newActivity);

        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Edit(string id)
        {
            var contact = contactRepository.GetSingleContactByEmail(id);
            contact.URLSafeEmail = contact.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            return View(contact);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult Edit(Contact editContact)
        {
            if (ModelState.IsValid)
            {
                using (var context = new CPTSContext())
                {
                    context.Entry(editContact).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return RedirectToAction("Details", new { id = editContact.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod) });
            }
            return View(editContact);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            var contact = contactRepository.GetSingleContactByEmail(id);
            contact.URLSafeEmail = id;
            return View(contact);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(string id)
        {
            string partnerName;
            using (var context = new CPTSContext())
            {
                var contact = context.Contacts.Find(id.Replace(ConstantsChars.ReplacementCharForPeriod, '.'));
                var partner = context.Partners.First(p => p.Contacts.Any(c => c.Email == contact.Email));
                partnerName = partner.PartnerName;
                context.Contacts.Remove(contact);
                context.SaveChanges();
            }
            return RedirectToAction("Details", "Partner", new { id = partnerName });
        }

        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateCampusUnitData(string id)
        {
            CreateCampusUnitData createCampusUnitData = new CreateCampusUnitData();
            var contact = new List<Contact> { contactRepository.GetSingleContactByEmail(id) };
            contact.First().URLSafeEmail = contact.First().Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            CampusUnitData campusUnitData = new CampusUnitData();
            var campusUnit = new CampusUnit();
            campusUnitData.CampusUnit = campusUnit;
            campusUnitData.CampusUnit.Name = "";
            campusUnitData.AssignedContact = contact.ElementAt(0);
            campusUnitData.AssignedContact.Email = id.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            createCampusUnitData.CampusUnitData = campusUnitData;
            createCampusUnitData.Semesters = GetSemesters();
            createCampusUnitData.CampusUnits = GetCampusUnitNames(campusUnitData.AssignedContact.Email); //Add a list of campus unit names for the user to select from

            return View(createCampusUnitData);
        }

        [HttpPost]
        [Authorize(Roles = "Privileged User, Admin")]
        public ActionResult CreateCampusUnitData(CreateCampusUnitData newCampusUnitData)
        {
            if (CheckForRequiredFieldsCampusUnitDataCreate(newCampusUnitData))
            {
                if (CheckForDuplicateKeyCampusUnitDataCreate(newCampusUnitData))
                {
                    using (var context = new CPTSContext())
                    {
                        newCampusUnitData.CampusUnitData.AssignedContact = context.Contacts.Find(newCampusUnitData.CampusUnitData.AssignedContact.Email);
                        context.Entry(newCampusUnitData.CampusUnitData.CampusUnit).State = EntityState.Unchanged;
                        context.CampusUnitDataRecords.Add(newCampusUnitData.CampusUnitData);
                        context.SaveChanges();
                    }
                    return RedirectToAction("Details", new { id = newCampusUnitData.CampusUnitData.AssignedContact.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod) });
                }               
            }
            var contact =  contactRepository.GetSingleContactByEmail(newCampusUnitData.CampusUnitData.AssignedContact.Email);
            contact.URLSafeEmail = contact.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            newCampusUnitData.CampusUnitData.AssignedContact = contact;
            newCampusUnitData.Semesters = GetSemesters();
            newCampusUnitData.CampusUnits = GetCampusUnitNames(newCampusUnitData.CampusUnitData.AssignedContact.Email);
            return View(newCampusUnitData);
        }

        private bool CheckForRequiredFields(CreateActivity newActivity)
        {
            bool correctFields = true;
            if (newActivity.CampusUnitName == null || newActivity.Activity.Subject == null || newActivity.Activity.Date == null)
            {
                ViewBag.Message = "You are missing required field(s)!";
                correctFields = false;
            }
            else if (newActivity.Activity.Date.Year < 1950)
            {                   
                ViewBag.Message = "Incorrect date format, or date out of range.";
                correctFields = false;
            }            
            return correctFields;
        }

        private bool CheckForDuplicateKeysActivityCreate(CreateActivity newActivity)
        {
            if (activityRepository.GetActivityFromSubjectAndDate(newActivity.Activity.Subject, newActivity.Activity.Date) == null)
            {
                return true;
            }
            ViewBag.Message = "An activity with that exact subject, date, and time already exists in the system.";
            return false;
        }

        private bool CheckForRequiredFieldsCampusUnitDataCreate(CreateCampusUnitData newCampusUnitData)
        {
            if (newCampusUnitData.CampusUnitData.CampusUnit.Name != null && newCampusUnitData.CampusUnitData.UWSProgram != null && newCampusUnitData.CampusUnitData.DepartmentEvent != null && newCampusUnitData.CampusUnitData.UWSStaff != null && newCampusUnitData.CampusUnitData.Semester != null)
            {
                return true;
            }
            ViewBag.Message = "You are missing required field(s)!";
            return false;
        }

        private bool CheckForDuplicateKeyCampusUnitDataCreate(CreateCampusUnitData newCampusUnitData)
        {
            if (campusUnitDataRepository.GetSingleCampusUnitDataRecord(newCampusUnitData.CampusUnitData.UWSProgram, newCampusUnitData.CampusUnitData.DepartmentEvent, newCampusUnitData.CampusUnitData.UWSStaff, newCampusUnitData.CampusUnitData.Semester, newCampusUnitData.CampusUnitData.AssignedContact.Email) == null)
            {
                return true;
            }
            ViewBag.Message = "That exact set of required fields already exists in the system.";
            return false;
        }

        private List<string> GetSemesters()
        {
            List<string> semesterList = new List<string>();
            for (int i = 0; i < 16; i++)                     //Yes 16 is a magic number, but we talked about having the past two semesters and the next three years available
            {
                string semester = "J-Term";
                if (i % 4 == 0)
                    semester = "Spring";
                else if (i % 4 == 1)
                    semester = "Summer";
                else if (i % 4 == 2)
                    semester = "Fall";

                DateTime date = DateTime.Now;
                int year = date.Year;
                year = year + ((i + 1) / 4) - 1; // A little integer division magic that gives us 4 terms with the same year

                semesterList.Add("" + semester + " " + year);
            }
            return semesterList;
        }

        private List<string> GetCampusUnitNames(string email)
        {
            var campusUnitList = campusUnitRepository.FindCampusUnitsForContactEmail(email); 
            List<string> campusUnitNameList = new List<string>();
            foreach (CampusUnit c in campusUnitList)
            {
                campusUnitNameList.Add(c.Name);
            }

            return campusUnitNameList;
        }
    }
}