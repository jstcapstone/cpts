﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Repository.Models;
using CPTS.Helpers;
using CPTS.Repository.Context;
using System.Text;
using CPTS.Repository.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class ActivitySearchController : Controller
    {
        ActivityRepository activityRepository = new ActivityRepository();
        PartnerRepository partnerRepository = new PartnerRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        ContactRepository contactRepository = new ContactRepository();
        SafeURL activityHelper = new SafeURL();

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ActivitySearch(string searchActivitySubject, string searchDateFrom, string searchDateTo)
        {
            var activityList = activityRepository.FindActivitiesBasedOnSubjectAndDate(searchActivitySubject, searchDateFrom, searchDateTo);
            foreach (var activity in activityList)
            {
                activity.URLSafeSubject = activityHelper.CreateUrlSafeSubject(activity.Subject);
                activity.Partner = partnerRepository.GetPartnerByActivity(activity);
                activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivityNonTruncateTime(activity);
                activity.SearchString = searchActivitySubject + ConstantsChars.CharForStringSeperator + searchDateFrom 
                    + ConstantsChars.CharForStringSeperator + searchDateTo;
            }
            ViewBag.Message = activityList.Count;

            return View(activityList);
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportActivities(string searchString)
        {       
            string csvFileString = "No search results to export.";

            if (searchString != null)
            {
                ExcelExport export = new ExcelExport();
                string[] searchStringArray = searchString.Split(ConstantsChars.CharForStringSeperator);

                List<Activity> activityList = activityRepository.FindActivitiesBasedOnSubjectAndDate(searchStringArray[0], searchStringArray[1], searchStringArray[2]);

                foreach (var activity in activityList)
                {
                    activity.URLSafeSubject = activityHelper.CreateUrlSafeSubject(activity.Subject);
                    activity.Partner = partnerRepository.GetPartnerByActivity(activity);
                    activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivityNonTruncateTime(activity);
                    activity.Contacts = contactRepository.FindContactsByActivity(activity);
                }
                csvFileString = export.ExportActivities(activityList);
            }

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportActivities.csv");
        }
    }
}
