﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CPTS.Helpers;
using CPTS.Repository.Context;
using CPTS.Repository.Models;
using System.Text;
using CPTS.Repository.Helpers;

namespace CPTS.Controllers
{
    [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
    public class ContactSearchController : Controller
    {
        ContactRepository contactRepository = new ContactRepository();
        PartnerRepository partnerRepository = new PartnerRepository();
        ActivityRepository activityRepository = new ActivityRepository();
        CampusUnitDataRepository campusUnitDataRepository = new CampusUnitDataRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();

        SafeURL emailHelper = new SafeURL();
        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ContactSearch(string searchContactName, string searchContactEmail, string searchPartnerName)
        {
            var contactList = contactRepository.FindContactsBasedOnNameEmailAndPartnerName(searchContactName, searchContactEmail, searchPartnerName);
            foreach (var contact in contactList)
            {
                contact.URLSafeEmail = emailHelper.CreateURLSafeEmail(contact.Email);
                contact.Partner = partnerRepository.GetPartnerByContactEmail(contact.Email);
                contact.SearchString = searchContactName + ConstantsChars.CharForStringSeperator + searchContactEmail
                    + ConstantsChars.CharForStringSeperator + searchPartnerName;
            }
            ViewBag.Message = contactList.Count;

            return View(contactList);
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportContacts(string searchString)
        {
            string csvFileString = Export(searchString, false);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportContacts.csv");
        }

        [Authorize(Roles = "Read-Only User, Privileged User, Admin")]
        public ActionResult ExportContactsCampusUnitData(string searchString)
        {
            string csvFileString = Export(searchString, true);

            return File(new UTF8Encoding().GetBytes(csvFileString), "text/csv", "ExcelExportContactsCampusUnitData.csv");
        }

        public string Export(string searchString, bool campusUnitData)
        {
            string csvFileString = "No search results to export.";

            if (searchString != null)
            {
                ExcelExport export = new ExcelExport();
                string[] searchStringArray = searchString.Split(ConstantsChars.CharForStringSeperator);

                List<Contact> contactList = contactRepository.FindContactsBasedOnNameEmailAndPartnerName(searchStringArray[0], searchStringArray[1], searchStringArray[2]);

                foreach (var contact in contactList)
                {
                    contact.Partner = partnerRepository.GetPartnerByContactEmail(contact.Email);
                    contact.Activities = activityRepository.FindActivitiesFromContactEmail(contact.Email);
                    contact.CampusUnitData = campusUnitDataRepository.FindCampusUnitDataFromContactEmail(contact.Email);

                    foreach (var data in contact.CampusUnitData)
                    {
                        data.AssignedContact = contactRepository.GetSingleContactByEmail(contact.Email);
                        data.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(data);
                    }
                }
                if (campusUnitData) csvFileString = export.ExportContactsCampusUnitData(contactList);
                else csvFileString = export.ExportContacts(contactList);
            }

            return csvFileString;
        }
    }
}
