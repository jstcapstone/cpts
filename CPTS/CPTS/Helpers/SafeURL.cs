﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CPTS.Repository.Helpers;

namespace CPTS.Helpers
{
    public class SafeURL
    {
        public string CreateURLSafeName(string unsafeName)
        {
            var safeString = unsafeName.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            safeString = safeString.Replace(':', ConstantsChars.ReplacementCharForColon);
            safeString = safeString.Replace('&', ConstantsChars.ReplacementCharForAnd);
            return safeString;
        }

        public string CreateNameFromSafeName(string safeName)
        {
            var name = safeName.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            name = name.Replace(ConstantsChars.ReplacementCharForColon, ':');
            name = name.Replace(ConstantsChars.ReplacementCharForAnd, '&');
            return name;
        }

        public string CreateURLSafeEmail(string unsafeEmail)
        {
            var safeEmail = unsafeEmail.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            return safeEmail;
        }

        public string CreateEmailFromSafeEmail(string safeEmail)
        {
            var email = safeEmail.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            return email;
        }

        public string CreateUrlSafeSubject(string unsafeSubject)
        {
            var safeSubject = unsafeSubject.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            safeSubject = safeSubject.Replace(':', ConstantsChars.ReplacementCharForColon);
            safeSubject = safeSubject.Replace('/', ConstantsChars.ReplacementCharForForwardSlash);
            return safeSubject;
        }
    }
}