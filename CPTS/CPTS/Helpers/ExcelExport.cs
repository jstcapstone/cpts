﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPTS.Repository.Context;
using CPTS.Models;
using CPTS.Repository.Models;

namespace CPTS.Helpers
{
    public class ExcelExport
    {
        TagRepository tagRepository = new TagRepository();
        ContactRepository contactRepository = new ContactRepository();
        CampusUnitRepository campusUnitRepository = new CampusUnitRepository();
        CampusUnitDataRepository campusUnitDataRepository = new CampusUnitDataRepository();

        public string ExportPartners(List<HomeModel> homeModel) // exporting just partners, one partner per row.
        {
            int maxTagCount = 0;
            foreach (var model in homeModel)
            {
                model.Partner.SearchTags = tagRepository.FindTagsFromPartnerName(model.Partner.PartnerName);
                model.CampusUnits = campusUnitRepository.FindCampusUnitsForPartner(model.Partner);
                if (maxTagCount < model.Partner.SearchTags.Count) maxTagCount = model.Partner.SearchTags.Count;
            }

            string csvFileString = "Community Partner Name,Website,Address,";
            for (int i = 1; i < maxTagCount + 1; i++)
            {
                csvFileString += "Tag " + i + ",";
            }
            csvFileString += "Notes,Campus Units,\n";

            foreach (var model in homeModel)
            {
                csvFileString += "\"" + model.Partner.PartnerName + "\",\"" + model.Partner.Website + "\",\"" + model.Partner.Address + "\",";

                int t = 0;
                if (model.Partner.SearchTags != null)
                {
                    for (; t < model.Partner.SearchTags.Count; t++)
                    {
                        csvFileString += model.Partner.SearchTags[t].TagName + ",";                      
                    }
                }
                for (; t < maxTagCount; t++) csvFileString += ",";

                csvFileString += "\"" + model.Partner.Notes + "\",\"";
                for (int k = 0; k < model.CampusUnits.Count; k++)
                {
                    csvFileString += model.CampusUnits[k].Name;
                    if (k < model.CampusUnits.Count - 1) csvFileString += ", ";
                }
                csvFileString += "\",\n";
            }

            return csvFileString;
        }

        public string ExportPartnersContacts(List<HomeModel> homeModel) // export partners with additional contacts listed on new rows for the same partner
        {            
            int maxTagCount = 0;
            int entryRowCount = 1;

            foreach (var model in homeModel)
            {
                model.Partner.Contacts = contactRepository.FindContactsFromPartnerName(model.Partner.PartnerName);
                model.Partner.SearchTags = tagRepository.FindTagsFromPartnerName(model.Partner.PartnerName);
                model.CampusUnits = campusUnitRepository.FindCampusUnitsForPartner(model.Partner);
                if (maxTagCount < model.Partner.SearchTags.Count) maxTagCount = model.Partner.SearchTags.Count;
            }

            string csvFileString = "Community Partner Name,Website,Address,";
            for (int i = 1; i < maxTagCount + 1; i++)
            {
                csvFileString += "Tag " + i + ",";
            }
            csvFileString += "Partner Notes,Campus Units,Contact First Name,Contact Last Name,Contact Email,Contact Phone,Contact Title,Contact Notes\n";


            foreach (var model in homeModel)
            {
                csvFileString += "\"" + model.Partner.PartnerName + "\",\"" + model.Partner.Website + "\",\"" + model.Partner.Address + "\",";

                int i = 0;
                if (model.Partner.SearchTags != null)
                {
                    for (; i < model.Partner.SearchTags.Count; i++)
                    {
                        csvFileString += model.Partner.SearchTags[i].TagName + ",";
                    }
                }
                for (; i < maxTagCount; i++) csvFileString += ",";

                csvFileString += "\"" + model.Partner.Notes + "\",\"";
                for (int j = 0; j < model.CampusUnits.Count; j++)
                {
                    csvFileString += model.CampusUnits[j].Name;
                    if (j < model.CampusUnits.Count - 1) csvFileString += ", ";
                }
                csvFileString += "\",";

                for (int k = 0; k < model.Partner.Contacts.Count; k++)
                {
                    if (k > 0)
                    {
                        entryRowCount++;
                        csvFileString += "\n\"" + model.Partner.PartnerName + "\"," + ",,,,,";
                        for (int j = 0; j < maxTagCount - 1; j++) csvFileString += ",";
                    }

                    csvFileString += "\"" + model.Partner.Contacts[k].FirstName + "\",\"" + model.Partner.Contacts[k].LastName + "\",\""
                        + model.Partner.Contacts[k].Email + "\",\"" + model.Partner.Contacts[k].PhoneNumber + "\",\"" 
                        + model.Partner.Contacts[k].Title + "\",\"" + model.Partner.Contacts[k].Notes + "\",";
                }
                entryRowCount++;
                csvFileString += "\n";
            }

            // needed to find the collumn letter because tag count is variable
            string[] excelColumns = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", 
                "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };

            csvFileString += "\nDistinct Community Partners,,,,,,,";
            for (int j = 0; j < maxTagCount; j++) csvFileString += ",";
            csvFileString += "Distinct Contacts,";

            string partnerCellRange = "A2:A" + entryRowCount;
            string contactCellRange = "" + excelColumns[7 + maxTagCount] + "2:" + excelColumns[7 + maxTagCount] + entryRowCount;

            csvFileString += "\n\"=SUMPRODUCT((" + partnerCellRange + "<>\"\"\"\")/COUNTIF(" + partnerCellRange + "," + partnerCellRange + "&\"\"\"\"))\",,,,,,,"; // hard coding excel formulas, summing distinct partners   
            for (int j = 0; j < maxTagCount; j++) csvFileString += ",";
            csvFileString += "\"=SUMPRODUCT((" + contactCellRange + "<>\"\"\"\")/COUNTIF(" + contactCellRange + "," + contactCellRange + "&\"\"\"\"))\","; // sum distinct contacts

            return csvFileString;
        }

        public string ExportPartnersContactsCampusUnitData(List<HomeModel> homeModel, string specificCampusUnit = null) // specificCampusUnit can be a campus unit string, and partners, contacts and data will only be printed for that unit
        {
            int maxTagCount = 0;
            int entryRowCount = 1;

            foreach (var model in homeModel)
            {
                model.Partner.Contacts = contactRepository.FindContactsFromPartnerName(model.Partner.PartnerName);
                model.Partner.SearchTags = tagRepository.FindTagsFromPartnerName(model.Partner.PartnerName);
                model.CampusUnits = campusUnitRepository.FindCampusUnitsForPartner(model.Partner);
                if (maxTagCount < model.Partner.SearchTags.Count) maxTagCount = model.Partner.SearchTags.Count;
            }

            string csvFileString = "Community Partner Name,Website,Address,";
            for (int i = 1; i < maxTagCount + 1; i++)
            {
                csvFileString += "Tag " + i + ",";
            }
            csvFileString += "Partner Notes,Campus Units,Contact Email,First Name,Last Name,Phone Number,Title,Contact Notes,Data: Campus Unit,"
                + "UWS Program/Course,Department/Event,UWS Staff/Instructor,Semester,Donated Hours,Student Count,Accomplishments,\n";

            foreach (var model in homeModel)
            {
                if (specificCampusUnit != null && !partnerHasSpecificCampusUnitData(model.Partner.Contacts, specificCampusUnit)) continue;

                csvFileString += "\"" + model.Partner.PartnerName + "\",\"" + model.Partner.Website + "\",\"" + model.Partner.Address + "\",";

                int t = 0;
                if (model.Partner.SearchTags != null)
                {
                    for (; t < model.Partner.SearchTags.Count; t++)
                    {
                        csvFileString += model.Partner.SearchTags[t].TagName + ",";
                    }
                }
                for (; t < maxTagCount; t++) csvFileString += ",";

                csvFileString += "\"" + model.Partner.Notes + "\",\"";
                for (int k = 0; k < model.CampusUnits.Count; k++)
                {
                    csvFileString += model.CampusUnits[k].Name;
                    if (k < model.CampusUnits.Count - 1) csvFileString += ", ";
                }
                csvFileString += "\",";

                int contactPlacement = 0;
                for (int j = 0; j < model.Partner.Contacts.Count; j++)
                {                   
                    if (specificCampusUnit != null && !contactHasSpecificCampusUnitData(model.Partner.Contacts[j], specificCampusUnit))
                    {
                        contactPlacement++;
                        continue;
                    }

                    if (j > contactPlacement)
                    {
                        entryRowCount++;
                        csvFileString += "\n\"" + model.Partner.PartnerName + "\",,,,,,";                        
                        for (int k = 0; k < maxTagCount - 1; k++) csvFileString += ",";
                    }

                    csvFileString += "\"" + model.Partner.Contacts[j].Email + "\",\"" + model.Partner.Contacts[j].FirstName + "\",\""
                        + model.Partner.Contacts[j].LastName + "\",\"" + model.Partner.Contacts[j].PhoneNumber 
                        + "\",\"" + model.Partner.Contacts[j].Title + "\",\"" + model.Partner.Contacts[j].Notes + "\",";

                    model.Partner.Contacts[j].CampusUnitData = campusUnitDataRepository.FindCampusUnitDataFromContactEmail(model.Partner.Contacts[j].Email);

                    int dataPlacement = 0;
                    if (model.Partner.Contacts[j].CampusUnitData != null)
                    {                       
                        for (int k = 0; k < model.Partner.Contacts[j].CampusUnitData.Count; k++)
                        {
                            var data = model.Partner.Contacts[j].CampusUnitData[k];
                            data.AssignedContact = contactRepository.GetSingleContactByEmail(model.Partner.Contacts[j].Email);
                            data.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(data);

                            if (specificCampusUnit != null && specificCampusUnit != data.CampusUnit.Name)
                            {
                                dataPlacement++;
                                continue;
                            }

                            if (k > dataPlacement)
                            {
                                entryRowCount++;
                                csvFileString += "\n\"" + model.Partner.PartnerName + "\",,,,,";
                                for (int l = 0; l < maxTagCount; l++) csvFileString += ",";
                                csvFileString += model.Partner.Contacts[j].Email + ",,,,,,";
                            }

                            csvFileString += "\"" + data.CampusUnit.Name + "\",\"" + data.UWSProgram + "\",\"" + data.DepartmentEvent + "\",\"" + data.UWSStaff
                                + "\",\"" + data.Semester + "\",\"" + data.DonatedHours + "\",\"" + data.StudentCount + "\",\"" + data.Accomplishment + "\",";
                        }
                    }
                }
                entryRowCount++;
                csvFileString += "\n";
            }

            string[] excelColumns = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };

            string partnerCellRange = "A2:A" + entryRowCount;                                                                                       
            string contactCellRange = "" + excelColumns[maxTagCount + 5] + "2:" + excelColumns[maxTagCount + 5] + entryRowCount;        
            string campusUnitCellRange = "" + excelColumns[maxTagCount + 11] + "2:" + excelColumns[maxTagCount + 11] + entryRowCount;
            string uwsProgramCellRange = "" + excelColumns[maxTagCount + 12] + "2:" + excelColumns[maxTagCount + 12] + entryRowCount;
            string departmentEventCellRange = "" + excelColumns[maxTagCount + 13] + "2:" + excelColumns[maxTagCount + 13] + entryRowCount;
            string uwsStaffCellRange = "" + excelColumns[maxTagCount + 14] + "2:" + excelColumns[maxTagCount + 14] + entryRowCount;
            string semesterCellRange = "" + excelColumns[maxTagCount + 15] + "2:" + excelColumns[maxTagCount + 15] + entryRowCount;

            csvFileString += "\nDistinct Community Partners,";
            for (int i = 0; i < maxTagCount + 4; i++) csvFileString += ",";
            csvFileString += "Distinct Contacts,,,,,,Distinct Campus Units,Distinct Programs/Courses,Distinct Departments/Events,"
                + "Distinct Staff/Instructors,Distinct Semesters,Total Donated Hours,Total Student Count,\n";

            csvFileString += "\"=SUMPRODUCT((" + partnerCellRange + "<>\"\"\"\")/COUNTIF(" + partnerCellRange + "," + partnerCellRange + "&\"\"\"\"))\",";  
            for (int i = 0; i < maxTagCount + 4; i++) csvFileString += ",";
            csvFileString += "\"=SUMPRODUCT((" + contactCellRange + "<>\"\"\"\")/COUNTIF(" + contactCellRange + "," + contactCellRange + "&\"\"\"\"))\",,,,,,"
                + "\"=SUMPRODUCT((" + campusUnitCellRange + "<>\"\"\"\")/COUNTIF(" + campusUnitCellRange + "," + campusUnitCellRange + "&\"\"\"\"))\","             // summing campus unit data fields
                + "\"=SUMPRODUCT((" + uwsProgramCellRange + "<>\"\"\"\")/COUNTIF(" + uwsProgramCellRange + "," + uwsProgramCellRange + "&\"\"\"\"))\","   
                + "\"=SUMPRODUCT((" + departmentEventCellRange + "<>\"\"\"\")/COUNTIF(" + departmentEventCellRange + "," + departmentEventCellRange + "&\"\"\"\"))\","  
                + "\"=SUMPRODUCT((" + uwsStaffCellRange + "<>\"\"\"\")/COUNTIF(" + uwsStaffCellRange + "," + uwsStaffCellRange + "&\"\"\"\"))\","
                + "\"=SUMPRODUCT((" + semesterCellRange + "<>\"\"\"\")/COUNTIF(" + semesterCellRange + "," + semesterCellRange + "&\"\"\"\"))\","
                + "=SUM(" + excelColumns[maxTagCount + 16] + "2:" + excelColumns[maxTagCount + 16] + entryRowCount + "),"
                + "=SUM(" + excelColumns[maxTagCount + 17] + "2:" + excelColumns[maxTagCount + 17] + entryRowCount + "),";

            return csvFileString;
        }

        public bool partnerHasSpecificCampusUnitData(List<Contact> contacts, string campusUnit)
        {
            foreach (var contact in contacts)
            {
                if (contactHasSpecificCampusUnitData(contact, campusUnit)) return true;
            }
            return false;
        }
        
        public bool contactHasSpecificCampusUnitData(Contact contact, string campusUnit)
        {
            contact.CampusUnitData = campusUnitDataRepository.FindCampusUnitDataFromContactEmail(contact.Email);
            if (contact.CampusUnitData != null)
            {
                foreach (var data in contact.CampusUnitData)
                {
                    data.AssignedContact = contactRepository.GetSingleContactByEmail(contact.Email);
                    data.CampusUnit = campusUnitRepository.GetCampusUnitForCampusUnitData(data);
                    if (data.CampusUnit.Name == campusUnit) return true;
                }
            }
            return false;
        }

        public string ExportContacts(List<Contact> contacts) // export single contact per row
        { 
            string csvFileString = "Contact First Name,Contact Last Name,Contact Email,Contact Phone,Contact Title,Contact Notes,"
                + "Community Partner Name,Community Partner Address,Number of Activities for Contact,Number of Campus Unit Data Entries for Contact\n";

            foreach (var contact in contacts)
            {
                int campusUnitDataCount = 0;
                if (contact.CampusUnitData != null) campusUnitDataCount = contact.CampusUnitData.Count;

                csvFileString += "\"" + contact.FirstName + "\",\"" + contact.LastName + "\",\"" + contact.Email + "\",\"" + contact.PhoneNumber 
                    + "\",\"" + contact.Title + "\",\"" + contact.Notes + "\",\"" + contact.Partner.PartnerName + "\",\"" + contact.Partner.Address 
                    + "\",\"" + contact.Activities.Count + "\",\"" + campusUnitDataCount + "\",\n";
            }

            return csvFileString;
        }

        public string ExportContactsCampusUnitData(List<Contact> contacts) // export contacts with additional campus unit data on new rows
        {           
            string csvFileString = "Contact First Name,Contact Last Name,Contact Email,Contact Phone,Contact Title,Contact Notes,Community Partner Name,"
                + "Data: Campus Unit,UWS Program/Course,Department/Event,UWS Staff/Instructor,Semester,Donated Hours,Student Count,Accomplishments,\n";

            int entryRowCount = 1;

            foreach (var contact in contacts)
            {
                csvFileString += "\"" + contact.FirstName + "\",\"" + contact.LastName + "\",\"" + contact.Email + "\",\"" + contact.PhoneNumber
                    + "\",\"" + contact.Title + "\",\"" + contact.Notes + "\",\"" + contact.Partner.PartnerName + "\",";

                if (contact.CampusUnitData != null)
                {
                    for (int i = 0; i < contact.CampusUnitData.Count; i++)
                    {
                        if (i > 0)
                        {
                            entryRowCount++;
                            csvFileString += "\n,,\"" + contact.Email + "\",,,,,";
                        }
                        var data = contact.CampusUnitData[i];

                        csvFileString += "\"" + data.CampusUnit.Name + "\",\"" + data.UWSProgram + "\",\"" + data.DepartmentEvent + "\",\"" + data.UWSStaff 
                            + "\",\"" + data.Semester + "\",\"" + data.DonatedHours + "\",\"" + data.StudentCount + "\",\"" + data.Accomplishment + "\",";
                    }
                }
                entryRowCount++;
                csvFileString += "\n";
            }

            string contactsCellRange = "C2:C" + entryRowCount;
            string campusUnitCellRange = "H2:H" + entryRowCount;
            string uwsProgramCellRange = "I2:I" + entryRowCount;
            string departmentEventCellRange = "J2:J" + entryRowCount;
            string uwsStaffCellRange = "K2:K" + entryRowCount;
            string semesterCellRange = "L2:L" + entryRowCount;

            csvFileString += "\n,,Distinct Contacts,,,,,Distinct Campus Units,Distinct Programs/Courses,Distinct Departments/Events,Distinct Staff/Instructors,"
                + "Distinct Semesters,Total Donated Hours,Total Student Count,"
                + "\n,,\"=SUMPRODUCT((" + contactsCellRange + "<>\"\"\"\")/COUNTIF(" + contactsCellRange + "," + contactsCellRange + "&\"\"\"\"))\",,,,,"
                + "\"=SUMPRODUCT((" + campusUnitCellRange + "<>\"\"\"\")/COUNTIF(" + campusUnitCellRange + "," + campusUnitCellRange + "&\"\"\"\"))\","
                + "\"=SUMPRODUCT((" + uwsProgramCellRange + "<>\"\"\"\")/COUNTIF(" + uwsProgramCellRange + "," + uwsProgramCellRange + "&\"\"\"\"))\","
                + "\"=SUMPRODUCT((" + departmentEventCellRange + "<>\"\"\"\")/COUNTIF(" + departmentEventCellRange + "," + departmentEventCellRange + "&\"\"\"\"))\","
                + "\"=SUMPRODUCT((" + uwsStaffCellRange + "<>\"\"\"\")/COUNTIF(" + uwsStaffCellRange + "," + uwsStaffCellRange + "&\"\"\"\"))\","     
                + "\"=SUMPRODUCT((" + semesterCellRange + "<>\"\"\"\")/COUNTIF(" + semesterCellRange + "," + semesterCellRange + "&\"\"\"\"))\","               
                + "=SUM(M2:M" + entryRowCount + ")," + "=SUM(N2:N" + entryRowCount + "),";

            return csvFileString;
        }

        public string ExportActivities(List<Activity> activities) //  export single activity per row, with variable number of contacts listed behind on the same row 
        {
            int maxContactCount = 0;
            foreach (var activity in activities)
            {
                if (activity.Contacts.Count > maxContactCount) maxContactCount = activity.Contacts.Count;
            }

            string csvFileString = "Activity Subject,Activity Date,Campus Unit,Community Partner,Activity Notes,";
            for (int i = 1; i < maxContactCount + 1; i++)
            {
                csvFileString += "Contact " + i + " First Name,Contact " + i + " Last Name,Contact " + i + " Email,";
            }
            csvFileString += "\n";

            foreach (var activity in activities) 
            {
                csvFileString += "\"" + activity.Subject + "\",\"" + activity.Date.ToString() + "\",\"" + activity.CampusUnit.Name 
                    + "\",\"" + activity.Partner.PartnerName + "\",\"" + activity.Notes + "\",";

                foreach (var contact in activity.Contacts)
                {
                    csvFileString += "\"" + contact.FirstName + "\",\"" + contact.LastName + "\",\"" + contact.Email + "\",";
                }
                csvFileString += "\n";
            }

            string campusUnitCellRange = "C2:C" + (activities.Count + 1);
            string partnerCellRange = "D2:D" + (activities.Count + 1);
            csvFileString += "\n,,Distinct Campus Units, Distinct Community Partners,";
            csvFileString += "\n,,\"=SUMPRODUCT((" + campusUnitCellRange + "<>\"\"\"\")/COUNTIF(" + campusUnitCellRange + "," + campusUnitCellRange + "&\"\"\"\"))\",";
            csvFileString += "\"=SUMPRODUCT((" + partnerCellRange + "<>\"\"\"\")/COUNTIF(" + partnerCellRange + "," + partnerCellRange + "&\"\"\"\"))\",";

            return csvFileString;
        }
    }
}
