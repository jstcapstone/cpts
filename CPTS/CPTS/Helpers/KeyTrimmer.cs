﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CPTS.Helpers
{
    public class KeyTrimmer
    {
        public static string TrimTrailingSpaces(string rawKey)
        {
            var refinedKey = "";
            char[] toRemove = new char[1];
            toRemove[0] = ' ';
            if (rawKey != null)
            {
                refinedKey = rawKey.TrimEnd(toRemove);
            }
            return refinedKey;
        }
    }
}