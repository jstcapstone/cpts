using CPTS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CPTS.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CPTS.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CPTS.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Roles.AddOrUpdate(r => r.Name,
                new IdentityRole("Read-Only User"),
                new IdentityRole("Privileged User"),
                new IdentityRole("Admin"));

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var passwordHash = new PasswordHasher();
            if (!context.Users.Any(u => u.UserName == "kbauman3@uwsuper.edu"))
            {
                var user = new ApplicationUser
                {
                    UserName = "kbauman3@uwsuper.edu",
                    Email = "kbauman3@uwsuper.edu",
                    PasswordHash = passwordHash.HashPassword("123456")
                };

                userManager.Create(user);
                userManager.AddToRole(user.Id, "Admin");
            }
        }
    }
}
