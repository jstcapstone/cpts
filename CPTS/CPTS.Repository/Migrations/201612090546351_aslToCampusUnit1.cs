namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aslToCampusUnit1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.CampusUnitDatas");
            AddColumn("dbo.CampusUnitDatas", "CampusUnit_Name", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.CampusUnitDatas", new[] { "UWSProgram", "DepartmentEvent", "UWSStaff", "Semester" });
            CreateIndex("dbo.CampusUnitDatas", "CampusUnit_Name");
            AddForeignKey("dbo.CampusUnitDatas", "CampusUnit_Name", "dbo.CampusUnits", "Name");
            DropColumn("dbo.CampusUnitDatas", "CampusUnitName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CampusUnitDatas", "CampusUnitName", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.CampusUnitDatas", "CampusUnit_Name", "dbo.CampusUnits");
            DropIndex("dbo.CampusUnitDatas", new[] { "CampusUnit_Name" });
            DropPrimaryKey("dbo.CampusUnitDatas");
            DropColumn("dbo.CampusUnitDatas", "CampusUnit_Name");
            AddPrimaryKey("dbo.CampusUnitDatas", new[] { "CampusUnitName", "UWSProgram", "DepartmentEvent", "UWSStaff", "Semester" });
        }
    }
}
