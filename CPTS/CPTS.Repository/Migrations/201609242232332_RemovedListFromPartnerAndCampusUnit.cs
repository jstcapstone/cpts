namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedListFromPartnerAndCampusUnit : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PartnerCampusUnits", "Partner_PartnerName", "dbo.Partners");
            DropForeignKey("dbo.PartnerCampusUnits", "CampusUnit_Name", "dbo.CampusUnits");
            DropIndex("dbo.PartnerCampusUnits", new[] { "Partner_PartnerName" });
            DropIndex("dbo.PartnerCampusUnits", new[] { "CampusUnit_Name" });
            AddColumn("dbo.Partners", "Address", c => c.String());
            AddColumn("dbo.Partners", "Website", c => c.String());
            DropColumn("dbo.Partners", "PartnerAddress");
            DropColumn("dbo.Partners", "PartnerEmailAddress");
            DropTable("dbo.PartnerCampusUnits");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PartnerCampusUnits",
                c => new
                    {
                        Partner_PartnerName = c.String(nullable: false, maxLength: 128),
                        CampusUnit_Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Partner_PartnerName, t.CampusUnit_Name });
            
            AddColumn("dbo.Partners", "PartnerEmailAddress", c => c.String());
            AddColumn("dbo.Partners", "PartnerAddress", c => c.String());
            DropColumn("dbo.Partners", "Website");
            DropColumn("dbo.Partners", "Address");
            CreateIndex("dbo.PartnerCampusUnits", "CampusUnit_Name");
            CreateIndex("dbo.PartnerCampusUnits", "Partner_PartnerName");
            AddForeignKey("dbo.PartnerCampusUnits", "CampusUnit_Name", "dbo.CampusUnits", "Name", cascadeDelete: true);
            AddForeignKey("dbo.PartnerCampusUnits", "Partner_PartnerName", "dbo.Partners", "PartnerName", cascadeDelete: true);
        }
    }
}
