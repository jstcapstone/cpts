using System.Collections.Generic;
using System.Net.Http.Headers;
using CPTS.Repository.Models;

namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CPTS.Repository.Context.CPTSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CPTS.Repository.Context.CPTSContext";
        }

        protected override void Seed(CPTS.Repository.Context.CPTSContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.CampusUnits.AddOrUpdate(c => c.Name, 
                new CampusUnit { Name = "AS-L"},
                new CampusUnit { Name = "URSCA"},
                new CampusUnit { Name = "Career Services"},
                new CampusUnit { Name = "Student Involvement" },
                new CampusUnit { Name = "Academic Department" },
                new CampusUnit { Name = "Athletics" },
                new CampusUnit { Name = "Multicultural Affairs" },
                new CampusUnit { Name = "Campus Recreation" }
                );

            context.Partners.AddOrUpdate(p => p.PartnerName,
                new Partner { PartnerName = "Center Against Sexual and Domestic Abuse (CASDA)", Address = "318 21st Ave E Superior, WI 54880 ", Website = "casda.org" },
                new Partner { PartnerName = "American Red Cross - Northland Chapter", Address = "2524 Maple Grove Rd, Duluth, MN  55811", Website = "www.redcross.org/local/minnesota/chapters/northern" },
                new Partner { PartnerName = "Bryant Elementary", Address = "1423 Central Ave Superior, WI 54880", Website = "bryant.superior.schoolfusion.us/" },
                new Partner { PartnerName = "Bryant PTO", Address = "1423 Central Ave Superior, WI 54880" },
                new Partner { PartnerName = "Lake Superior Elementary", Address = "6200 E 3rd St, Superior, WI 54880", Website = "lakesuperior.superior.schoolfusion.us/modules/cms/announce.phtml" },
                new Partner { PartnerName = "Lake Superior PTA", Address = "6200 E 3rd St, Superior, WI 54880", Website = "school-newspaper.lakesuperior.superior.schoolfusion.us/modules/groups/integrated_home.phtml?&gid=2236286" },
                new Partner { PartnerName = "Denfeld High School", Address = "401 N 44th Ave W, Duluth, MN 55807", Website = "denfeld-site.isd709.org/" },
                new Partner { PartnerName = "Noah's Ark Daycare", Address = "1625 N 59th St, Superior, WI  54880", Website = "sites.google.com/site/nadccenter/" },
                new Partner { PartnerName = "Superior Children's Center", Address = "2416 Hill Ave, Superior, WI  54880", Website = "superior-community-preschool.superior.schoolfusion.us/modules/groups/group_pages.phtml?gid=2259065&nid=214707&sessionid=" },
                new Partner { PartnerName = "Upward Bound", Address = "UW-Superior, Old Main #130", Website = "www.uwsuper.edu/ub/index.cfm" },
                new Partner { PartnerName = "YWCA Girl Power!", Address = "32 E First Street, Suite 202, Duluth, MN", Website = "www.ywcaduluth.org/girl-power/" },
                new Partner { PartnerName = "Boys & Girls Club of Duluth (Lincoln Park)", Address = "2407 West 4th Street, Duluth, MN 55806", Website = "bgcnorth.org/locations/lincoln-park/" },
                new Partner { PartnerName = "23rd Veteran", Address = "178 Nynas Road, Esko, MN 55733", Website = "23rdveteran.org/" },
                new Partner { PartnerName = "Lake Superior National Estuarine Research Reserve", Address = "14 Marina Dr. Superior WI 54880", Website = "lsnerr.uwex.edu/" }
                );



            var HealthWellness = new List<Partner> { context.Partners.Find("Center Against Sexual and Domestic Abuse (CASDA)"), context.Partners.Find("23rd Veteran") };
            var Poverty = new List<Partner> { context.Partners.Find("Center Against Sexual and Domestic Abuse (CASDA)") };
            var Empowerment = new List<Partner>
            {
                context.Partners.Find("Center Against Sexual and Domestic Abuse (CASDA)"),
                context.Partners.Find("Upward Bound"),
                context.Partners.Find("YWCA Girl Power!"),
                context.Partners.Find("Boys & Girls Club of Duluth (Lincoln Park)"),
                context.Partners.Find("23rd Veteran")
            };
            var EducationYouthKidsFamily = new List<Partner>
            {
                context.Partners.Find("American Red Cross - Northland Chapter"),
                context.Partners.Find("Bryant Elementary"),
                context.Partners.Find("Bryant PTO"),
                context.Partners.Find("Lake Superior Elementary"),
                context.Partners.Find("Lake Superior PTA"),
                context.Partners.Find("Denfeld High School"),
                context.Partners.Find("Noah's Ark Daycare"),
                context.Partners.Find("Superior Children's Center"),
                context.Partners.Find("Upward Bound"),
                context.Partners.Find("YWCA Girl Power!"),
                context.Partners.Find("Boys & Girls Club of Duluth (Lincoln Park)")
            };
            var Religious = new List<Partner> { context.Partners.Find("Noah's Ark Daycare") };
            var NaturalResources = new List<Partner> {context.Partners.Find("Lake Superior National Estuarine Research Reserve") };
            List<Partner> Education = new List<Partner>
            {
                context.Partners.Find("American Red Cross - Northland Chapter"),
                context.Partners.Find("Bryant Elementary"),
                context.Partners.Find("Bryant PTO"),
                context.Partners.Find("Lake Superior Elementary"),
                context.Partners.Find("Lake Superior PTA"),
                context.Partners.Find("Denfeld High School"),
                context.Partners.Find("Noah's Ark Daycare"),
                context.Partners.Find("Superior Children's Center"),
                context.Partners.Find("Upward Bound"),
                context.Partners.Find("YWCA Girl Power!"),
                context.Partners.Find("Boys & Girls Club of Duluth (Lincoln Park)"),
                context.Partners.Find("23rd Veteran")
            };


            context.Tags.AddOrUpdate(c => c.TagName,
                new Tag { TagName = "" },
                new Tag { TagName = "Youth", Partners = EducationYouthKidsFamily},
                new Tag { TagName = "Diversity" },
                new Tag { TagName = "Education", Partners = Education},
                new Tag { TagName = "Community & Economic Development" },
                new Tag { TagName = "Public Safety" },
                new Tag { TagName = "Arts & Culture" },
                new Tag { TagName = "Environmental & Natural Resources" },
                new Tag { TagName = "Health & Wellness", Partners = HealthWellness},
                new Tag { TagName = "Animals" },
                new Tag { TagName = "Poverty", Partners = Poverty},
                new Tag { TagName = "Elderly" },
                new Tag { TagName = "Empowerment", Partners = Empowerment},
                new Tag { TagName = "Miscellaneous/Other" },
                new Tag { TagName = "Kids & Families", Partners = EducationYouthKidsFamily},
                new Tag { TagName = "Religious Sector", Partners = Religious},
                new Tag { TagName = "Government" },
                new Tag { TagName = "Business" },
                new Tag { TagName = "Vote/Elections" },
                new Tag { TagName = "Veterans" },
                new Tag { TagName = "Human Services" },
                new Tag { TagName = "Natural Resources", Partners = NaturalResources},
                new Tag { TagName = "Transportation" },
                new Tag { TagName = "UW-Superior" },
                new Tag { TagName = "Disability Services" }
                );

            context.Contacts.AddOrUpdate(c => c.Email,
                new Contact
                {
                    FirstName = "Jill",
                    LastName = "Hinners",
                    Email = "jill@casda.org",
                    PhoneNumber = "715-392-3136",
                    Title = "Community Engagement Coordinator",
                    Partner = context.Partners.Find("Center Against Sexual and Domestic Abuse (CASDA)")
                },
                new Contact
                {
                    FirstName = "Dan",
                    LastName = "Williams",
                    Email = "dan.williams@redcross.org",
                    PhoneNumber = "218-722-0071",
                    Title = "Executive Director",
                    Partner = context.Partners.Find("American Red Cross - Northland Chapter")
                },
                new Contact
                {
                    FirstName = "Kate",
                    LastName = "Tesch",
                    Email = "kate.tesch@superior.k12.wi.us",
                    PhoneNumber = "715-394-8785",
                    Title = "Principal",
                    Partner = context.Partners.Find("Bryant Elementary")
                },
                new Contact
                {
                    FirstName = "Kalee",
                    LastName = "Hermanson",
                    Email = "Kalee.H@hotmail.com",
                    PhoneNumber = "715-394-8785",
                    Title = "PTO President",
                    Partner = context.Partners.Find("Bryant PTO")
                },
                new Contact
                {
                    FirstName = "Mark",
                    LastName = "Howard",
                    Email = "Mark.Howard@Superior.k12.wi.us",
                    PhoneNumber = "715-398-7672",
                    Title = "Principal",
                    Partner = context.Partners.Find("Lake Superior Elementary")
                },
                new Contact
                {
                    FirstName = "Karen",
                    LastName = "Urban",
                    Email = "karenurban102@gmail.com",
                    PhoneNumber = "715-398-7672",
                    Title = "PTA President",
                    Partner = context.Partners.Find("Lake Superior PTA")
                },
                new Contact
                {
                    FirstName = "Michael",
                    LastName = "Meyer",
                    Email = "michael.meyer@isd709.org",
                    PhoneNumber = "218-336-8830",
                    Title = "Social Worker",
                    Partner = context.Partners.Find("Denfeld High School")
                },
                new Contact
                {
                    FirstName = "Cathy",
                    LastName = "Schweikert",
                    Email = "nadccenter@yahoo.com",
                    PhoneNumber = "715-392-1555",
                    Partner = context.Partners.Find("Noah's Ark Daycare")
                },
                new Contact
                {
                    FirstName = "Brenda",
                    LastName = "Geisler",
                    Email = "superiorchildrenscenter@yahoo.com",
                    PhoneNumber = "715-395-1933",
                    Title = "Director",
                    Partner = context.Partners.Find("Superior Children's Center")
                },
                new Contact
                {
                    FirstName = "Justin",
                    LastName = "Markon",
                    Email = "jmarkon4@uwsuper.edu",
                    PhoneNumber = "715-394-8232",
                    Title = "Upward Bound Coordinator",
                    Partner = context.Partners.Find("Upward Bound")
                },
                new Contact
                {
                    FirstName = "Mariel",
                    LastName = "Jordan",
                    Email = "Mariel@ywcaduluth.org",
                    PhoneNumber = "218-722-7425 x102",
                    Title = "Youth Worker, Volunteer Coordinator, True North AmeriCorps",
                    Partner = context.Partners.Find("YWCA Girl Power!")
                },
                new Contact
                {
                    FirstName = "Jayde",
                    LastName = "Kalkbrenner",
                    Email = "jkalkbrenner@bgcnorth.org",
                    PhoneNumber = "218-725-7706 ex.518",
                    Title = "Mentor Duluth Advocate/Volunteer Coordinator",
                    Partner = context.Partners.Find("Boys & Girls Club of Duluth (Lincoln Park)")
                },
                new Contact
                {
                    FirstName = "Mike",
                    LastName = "Waldron",
                    Email = "mike.waldron@23rdveteran.org",
                    PhoneNumber = "218-355-8364",
                    Title = "Executive Director",
                    Partner = context.Partners.Find("23rd Veteran")
                },
                new Contact
                {
                    FirstName = "Deanna",
                    LastName = "Erickson",
                    Email = "deanna.erickson@ces.uwex.edu",
                    PhoneNumber = "715-919-2154",
                    Title = "Education Coordinator",
                    Partner = context.Partners.Find("Lake Superior National Estuarine Research Reserve")
                }
            );

            var date = DateTime.Now;
            date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);

            var contact1 = new List<Contact> {context.Contacts.Find("jill@casda.org") };
            var contact2 = new List<Contact> { context.Contacts.Find("dan.williams@redcross.org") };
            var contact3 = new List<Contact> { context.Contacts.Find("kate.tesch@superior.k12.wi.us") };
            var contact4 = new List<Contact> { context.Contacts.Find("Kalee.H@hotmail.com") };
            var contact5 = new List<Contact> { context.Contacts.Find("Mark.Howard@Superior.k12.wi.us") };
            var contact6 = new List<Contact> { context.Contacts.Find("karenurban102@gmail.com") };
            var contact7 = new List<Contact> { context.Contacts.Find("michael.meyer@isd709.org") };
            var contact8 = new List<Contact> { context.Contacts.Find("nadccenter@yahoo.com") };
            var contact9 = new List<Contact> { context.Contacts.Find("superiorchildrenscenter@yahoo.com") };
            var contact10 = new List<Contact> { context.Contacts.Find("jmarkon4@uwsuper.edu") };
            var contact11 = new List<Contact> { context.Contacts.Find("Mariel@ywcaduluth.org") };
            var contact12 = new List<Contact> { context.Contacts.Find("jkalkbrenner@bgcnorth.org") };
            var contact13 = new List<Contact> { context.Contacts.Find("mike.waldron@23rdveteran.org") };
            var contact14 = new List<Contact> { context.Contacts.Find("deanna.erickson@ces.uwex.edu") };

            context.Activities.AddOrUpdate(a => a.Subject,
                new Activity
                {
                    Partner = context.Partners.Find("Center Against Sexual and Domestic Abuse (CASDA)"),
                    Contacts = contact1,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(1),
                    Subject = "Meet with Center Against Sexual and Domestic Abuse (CASDA)"
                },
                new Activity
                {
                    Partner = context.Partners.Find("American Red Cross - Northland Chapter"),
                    Contacts = contact2,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(2),
                    Subject = "Meet with American Red Cross - Northland Chapter"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Bryant Elementary"),
                    Contacts = contact3,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(3),
                    Subject = "Meet with Bryant Elementary"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Bryant PTO"),
                    Contacts = contact4,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(4),
                    Subject = "Meet with Bryant PTO"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Lake Superior Elementary"),
                    Contacts = contact5,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(5),
                    Subject = "Meet with Lake Superior Elementary"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Lake Superior PTA"),
                    Contacts = contact6,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(6),
                    Subject = "Meet with Lake Superior PTA"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Denfeld High School"),
                    Contacts = contact7,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(7),
                    Subject = "Meet with Denfeld High School"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Noah's Ark Daycare"),
                    Contacts = contact8,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(8),
                    Subject = "Meet with Noah's Ark Daycare"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Superior Children's Center"),
                    Contacts = contact9,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(9),
                    Subject = "Meet with Superior Children's Center"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Upward Bound"),
                    Contacts = contact10,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(10),
                    Subject = "Meet with Upward Bound"
                },
                new Activity
                {
                    Partner = context.Partners.Find("YWCA Girl Power!"),
                    Contacts = contact11,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(11),
                    Subject = "Meet with YWCA Girl Power!"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Boys & Girls Club of Duluth (Lincoln Park)"),
                    Contacts = contact12,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(12),
                    Subject = "Meet with Boys & Girls Club of Duluth (Lincoln Park)"
                },
                new Activity
                {
                    Partner = context.Partners.Find("23rd Veteran"),
                    Contacts = contact13,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(13),
                    Subject = "Meet with 23rd Veteran"
                },
                new Activity
                {
                    Partner = context.Partners.Find("Lake Superior National Estuarine Research Reserve"),
                    Contacts = contact14,
                    CampusUnit = context.CampusUnits.Find("AS-L"),
                    Date = date.AddMinutes(14),
                    Subject = "Meet with Lake Superior National Estuarine Research Reserve"
                }
                );
        }
    }
}
