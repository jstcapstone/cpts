namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPartnerUserContactActivityCampusUnit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Subject = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Notes = c.String(),
                        CampusUnit_Name = c.String(maxLength: 128),
                        Partner_PartnerName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Subject, t.Date })
                .ForeignKey("dbo.CampusUnits", t => t.CampusUnit_Name)
                .ForeignKey("dbo.Partners", t => t.Partner_PartnerName)
                .Index(t => t.CampusUnit_Name)
                .Index(t => t.Partner_PartnerName);
            
            CreateTable(
                "dbo.CampusUnits",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.String(),
                        Title = c.String(),
                        Notes = c.String(),
                        Partner_PartnerName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Email)
                .ForeignKey("dbo.Partners", t => t.Partner_PartnerName)
                .Index(t => t.Partner_PartnerName);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Email);
            
            CreateTable(
                "dbo.PartnerCampusUnits",
                c => new
                    {
                        Partner_PartnerName = c.String(nullable: false, maxLength: 128),
                        CampusUnit_Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Partner_PartnerName, t.CampusUnit_Name })
                .ForeignKey("dbo.Partners", t => t.Partner_PartnerName, cascadeDelete: true)
                .ForeignKey("dbo.CampusUnits", t => t.CampusUnit_Name, cascadeDelete: true)
                .Index(t => t.Partner_PartnerName)
                .Index(t => t.CampusUnit_Name);
            
            CreateTable(
                "dbo.ContactActivities",
                c => new
                    {
                        Contact_Email = c.String(nullable: false, maxLength: 128),
                        Activity_Subject = c.String(nullable: false, maxLength: 128),
                        Activity_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.Contact_Email, t.Activity_Subject, t.Activity_Date })
                .ForeignKey("dbo.Contacts", t => t.Contact_Email, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => new { t.Activity_Subject, t.Activity_Date }, cascadeDelete: true)
                .Index(t => t.Contact_Email)
                .Index(t => new { t.Activity_Subject, t.Activity_Date });
            
            AddColumn("dbo.Partners", "Notes", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "Partner_PartnerName", "dbo.Partners");
            DropForeignKey("dbo.Contacts", "Partner_PartnerName", "dbo.Partners");
            DropForeignKey("dbo.ContactActivities", new[] { "Activity_Subject", "Activity_Date" }, "dbo.Activities");
            DropForeignKey("dbo.ContactActivities", "Contact_Email", "dbo.Contacts");
            DropForeignKey("dbo.PartnerCampusUnits", "CampusUnit_Name", "dbo.CampusUnits");
            DropForeignKey("dbo.PartnerCampusUnits", "Partner_PartnerName", "dbo.Partners");
            DropForeignKey("dbo.Activities", "CampusUnit_Name", "dbo.CampusUnits");
            DropIndex("dbo.ContactActivities", new[] { "Activity_Subject", "Activity_Date" });
            DropIndex("dbo.ContactActivities", new[] { "Contact_Email" });
            DropIndex("dbo.PartnerCampusUnits", new[] { "CampusUnit_Name" });
            DropIndex("dbo.PartnerCampusUnits", new[] { "Partner_PartnerName" });
            DropIndex("dbo.Contacts", new[] { "Partner_PartnerName" });
            DropIndex("dbo.Activities", new[] { "Partner_PartnerName" });
            DropIndex("dbo.Activities", new[] { "CampusUnit_Name" });
            DropColumn("dbo.Partners", "Notes");
            DropTable("dbo.ContactActivities");
            DropTable("dbo.PartnerCampusUnits");
            DropTable("dbo.Users");
            DropTable("dbo.Contacts");
            DropTable("dbo.CampusUnits");
            DropTable("dbo.Activities");
        }
    }
}
