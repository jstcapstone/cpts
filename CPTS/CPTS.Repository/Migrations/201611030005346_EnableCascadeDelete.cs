namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnableCascadeDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Activities", "Partner_PartnerName", "dbo.Partners");
            DropIndex("dbo.Activities", new[] { "Partner_PartnerName" });
            AlterColumn("dbo.Activities", "Partner_PartnerName", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Activities", "Partner_PartnerName");
            AddForeignKey("dbo.Activities", "Partner_PartnerName", "dbo.Partners", "PartnerName", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "Partner_PartnerName", "dbo.Partners");
            DropIndex("dbo.Activities", new[] { "Partner_PartnerName" });
            AlterColumn("dbo.Activities", "Partner_PartnerName", c => c.String(maxLength: 128));
            CreateIndex("dbo.Activities", "Partner_PartnerName");
            AddForeignKey("dbo.Activities", "Partner_PartnerName", "dbo.Partners", "PartnerName");
        }
    }
}
