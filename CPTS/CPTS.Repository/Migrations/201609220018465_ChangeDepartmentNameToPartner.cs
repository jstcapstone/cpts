namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDepartmentNameToPartner : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Partners",
                c => new
                    {
                        PartnerName = c.String(nullable: false, maxLength: 128),
                        PartnerAddress = c.String(),
                        PartnerEmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.PartnerName);
            
            DropTable("dbo.Departments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentName = c.String(nullable: false, maxLength: 128),
                        DepartmentAddress = c.String(),
                        DepartmentEmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.DepartmentName);
            
            DropTable("dbo.Partners");
        }
    }
}
