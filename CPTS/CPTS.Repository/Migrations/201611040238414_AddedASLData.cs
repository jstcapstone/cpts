namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedASLData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ASLDatas",
                c => new
                    {
                        Course = c.String(nullable: false, maxLength: 128),
                        Instructor = c.String(nullable: false, maxLength: 128),
                        Semester = c.String(nullable: false, maxLength: 128),
                        DonatedHours = c.String(),
                        Accomplished = c.String(),
                    })
                .PrimaryKey(t => new { t.Course, t.Instructor, t.Semester });
            
            CreateTable(
                "dbo.ASLDataContacts",
                c => new
                    {
                        ASLData_Course = c.String(nullable: false, maxLength: 128),
                        ASLData_Instructor = c.String(nullable: false, maxLength: 128),
                        ASLData_Semester = c.String(nullable: false, maxLength: 128),
                        Contact_Email = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ASLData_Course, t.ASLData_Instructor, t.ASLData_Semester, t.Contact_Email })
                .ForeignKey("dbo.ASLDatas", t => new { t.ASLData_Course, t.ASLData_Instructor, t.ASLData_Semester }, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.Contact_Email, cascadeDelete: true)
                .Index(t => new { t.ASLData_Course, t.ASLData_Instructor, t.ASLData_Semester })
                .Index(t => t.Contact_Email);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ASLDataContacts", "Contact_Email", "dbo.Contacts");
            DropForeignKey("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" }, "dbo.ASLDatas");
            DropIndex("dbo.ASLDataContacts", new[] { "Contact_Email" });
            DropIndex("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" });
            DropTable("dbo.ASLDataContacts");
            DropTable("dbo.ASLDatas");
        }
    }
}
