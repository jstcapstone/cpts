namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAcademicDepartments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AcademicPrograms",
                c => new
                    {
                        ProgramName = c.String(nullable: false, maxLength: 128),
                        Department_DepartmentName = c.String(maxLength: 128),
                        Contact_Email = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProgramName)
                .ForeignKey("dbo.AcademicDepartments", t => t.Department_DepartmentName)
                .ForeignKey("dbo.Contacts", t => t.Contact_Email)
                .Index(t => t.Department_DepartmentName)
                .Index(t => t.Contact_Email);
            
            CreateTable(
                "dbo.AcademicDepartments",
                c => new
                    {
                        DepartmentName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.DepartmentName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AcademicPrograms", "Contact_Email", "dbo.Contacts");
            DropForeignKey("dbo.AcademicPrograms", "Department_DepartmentName", "dbo.AcademicDepartments");
            DropIndex("dbo.AcademicPrograms", new[] { "Contact_Email" });
            DropIndex("dbo.AcademicPrograms", new[] { "Department_DepartmentName" });
            DropTable("dbo.AcademicDepartments");
            DropTable("dbo.AcademicPrograms");
        }
    }
}
