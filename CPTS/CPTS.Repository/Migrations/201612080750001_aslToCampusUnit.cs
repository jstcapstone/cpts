namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aslToCampusUnit : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" }, "dbo.ASLDatas");
            DropForeignKey("dbo.ASLDataContacts", "Contact_Email", "dbo.Contacts");
            DropIndex("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" });
            DropIndex("dbo.ASLDataContacts", new[] { "Contact_Email" });
            CreateTable(
                "dbo.CampusUnitDatas",
                c => new
                    {
                        CampusUnitName = c.String(nullable: false, maxLength: 128),
                        UWSProgram = c.String(nullable: false, maxLength: 128),
                        DepartmentEvent = c.String(nullable: false, maxLength: 128),
                        UWSStaff = c.String(nullable: false, maxLength: 128),
                        Semester = c.String(nullable: false, maxLength: 128),
                        DonatedHours = c.String(),
                        StudentCount = c.String(),
                        Accomplishment = c.String(),
                        AssignedContact_Email = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.CampusUnitName, t.UWSProgram, t.DepartmentEvent, t.UWSStaff, t.Semester })
                .ForeignKey("dbo.Contacts", t => t.AssignedContact_Email, cascadeDelete: true)
                .Index(t => t.AssignedContact_Email);
            
            DropTable("dbo.ASLDatas");
            DropTable("dbo.ASLDataContacts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ASLDataContacts",
                c => new
                    {
                        ASLData_Course = c.String(nullable: false, maxLength: 128),
                        ASLData_Instructor = c.String(nullable: false, maxLength: 128),
                        ASLData_Semester = c.String(nullable: false, maxLength: 128),
                        Contact_Email = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ASLData_Course, t.ASLData_Instructor, t.ASLData_Semester, t.Contact_Email });
            
            CreateTable(
                "dbo.ASLDatas",
                c => new
                    {
                        Course = c.String(nullable: false, maxLength: 128),
                        Instructor = c.String(nullable: false, maxLength: 128),
                        Semester = c.String(nullable: false, maxLength: 128),
                        DonatedHours = c.String(),
                        Accomplished = c.String(),
                    })
                .PrimaryKey(t => new { t.Course, t.Instructor, t.Semester });
            
            DropForeignKey("dbo.CampusUnitDatas", "AssignedContact_Email", "dbo.Contacts");
            DropIndex("dbo.CampusUnitDatas", new[] { "AssignedContact_Email" });
            DropTable("dbo.CampusUnitDatas");
            CreateIndex("dbo.ASLDataContacts", "Contact_Email");
            CreateIndex("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" });
            AddForeignKey("dbo.ASLDataContacts", "Contact_Email", "dbo.Contacts", "Email", cascadeDelete: true);
            AddForeignKey("dbo.ASLDataContacts", new[] { "ASLData_Course", "ASLData_Instructor", "ASLData_Semester" }, "dbo.ASLDatas", new[] { "Course", "Instructor", "Semester" }, cascadeDelete: true);
        }
    }
}
