namespace CPTS.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.TagName);
            
            CreateTable(
                "dbo.TagPartners",
                c => new
                    {
                        Tag_TagName = c.String(nullable: false, maxLength: 128),
                        Partner_PartnerName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Tag_TagName, t.Partner_PartnerName })
                .ForeignKey("dbo.Tags", t => t.Tag_TagName, cascadeDelete: true)
                .ForeignKey("dbo.Partners", t => t.Partner_PartnerName, cascadeDelete: true)
                .Index(t => t.Tag_TagName)
                .Index(t => t.Partner_PartnerName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagPartners", "Partner_PartnerName", "dbo.Partners");
            DropForeignKey("dbo.TagPartners", "Tag_TagName", "dbo.Tags");
            DropIndex("dbo.TagPartners", new[] { "Partner_PartnerName" });
            DropIndex("dbo.TagPartners", new[] { "Tag_TagName" });
            DropTable("dbo.TagPartners");
            DropTable("dbo.Tags");
        }
    }
}
