﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class CampusUnit
    {
        [Key]
        public string Name { get; set; }

        public List<Activity> Activities { get; set; }

        public List<CampusUnitData> CampusUnitDataRecords { get; set; }
    }
}
