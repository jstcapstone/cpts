﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class Activity
    {
        [Required(ErrorMessage = "Activity subject is required")]
        [Key, Column(Order = 0)]
        public string Subject { get; set; }

        [Key, Column(Order = 1)]
        public CampusUnit CampusUnit { get; set; }

        [Key, Column(Order = 2), Required]
        public Partner Partner { get; set; }

        [Required(ErrorMessage = "Activity date is required")]
        [Key, Column(Order = 3)]
        public DateTime Date { get; set; }

        public List<Contact> Contacts { get; set; }
        
        public string Notes { get; set; }

        [NotMapped]
        public string ContactEmail { get; set; }

        [NotMapped]
        public string URLSafeSubject { get; set; }

        [NotMapped]
        public string SearchString { get; set; }
    }
}
