﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class Contact
    {
        [Key]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})[ a-z0-9.]*", ErrorMessage = "Not a valid Phone number")]
        public string PhoneNumber { get; set; }

        public string Title { get; set; }

        public string Notes { get; set; }

        public List<Activity> Activities { get; set; }

        public List<CampusUnitData> CampusUnitData { get; set; }

        public List<AcademicProgram> AcademicPrograms { get; set; }
        
        public Partner Partner { get; set; }

        [NotMapped]
        public string URLSafeEmail { get; set; }

        [NotMapped]
        public string SearchString { get; set; }
    }
}
