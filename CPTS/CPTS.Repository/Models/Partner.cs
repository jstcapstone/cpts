﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class Partner
    {
        [Key]
        [Required(ErrorMessage = "Partner name is required")]
        [RegularExpression(@"^[a-zA-Z0-9,.:!()& '-]+[^ ]", ErrorMessage = "Not a valid Partner Name. Only Letters, Numbers, and ,.:!()-'& allowed")]
        public string PartnerName { get; set; }

        public string Address { get; set; }

        public string Website { get; set; }

        public string Notes { get; set; }
        
        public List<Contact> Contacts { get; set; }

        public List<Tag> SearchTags { get; set; }

        public List<Activity> Activities { get; set; }

        [NotMapped]
        public string SearchString { get; set; }

        [NotMapped]
        public string tagString { get; set; }

        [NotMapped]
        public string campusUnitString { get; set; }

        [NotMapped]
        public string URLSafeName { get; set; }
    }
}
