﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class AcademicProgram
    {
        [Key, Column(Order = 0)]
        public string ProgramName { get; set; }

        [Key, Column(Order = 1)]
        public AcademicDepartment Department { get; set; }
    }
}
