﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Models
{
    public class CampusUnitData
    {
        [Key, Column(Order = 0)]
        public CampusUnit CampusUnit { get; set; }

        [Key, Column(Order = 1)]
        public string UWSProgram { get; set; }

        [Key, Column(Order = 2)]
        public string DepartmentEvent { get; set; }

        [Key, Column(Order = 3)]
        public string UWSStaff { get; set; }

        [Key, Column(Order = 4)]
        public string Semester { get; set; }

        [Required]
        [Key, Column(Order = 5)]
        public Contact AssignedContact { get; set; }

        public string DonatedHours { get; set; }

        public string StudentCount { get; set; }

        public string Accomplishment { get; set; }

        
    }
}
