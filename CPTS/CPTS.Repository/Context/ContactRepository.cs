﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;
using System.Data.Entity;
using CPTS.Repository.Helpers;

namespace CPTS.Repository.Context
{
    public class ContactRepository : Repository<Contact>
    {      
        public List<Contact> FindContactsFromPartnerName(string partnerName)
        {
            List<Contact> contactList = DbSet.Where(c => c.Partner.PartnerName == partnerName).ToList();

            foreach (var item in contactList)
            {
                item.URLSafeEmail = item.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            }

            return contactList;
        }

        public List<Contact> FindContactsFromPartnerName(Partner partner)
        {
            List<Contact> contactList = DbSet.Where(c => c.Partner.PartnerName == partner.PartnerName).ToList();

            foreach (var item in contactList)
            {
                item.Partner.PartnerName = partner.PartnerName;
                item.URLSafeEmail = item.Email.Replace('.', ConstantsChars.ReplacementCharForPeriod);
            }

            return contactList;
        }


        public Contact GetSingleContactByEmail(string safeEmail)
        {
            var email = safeEmail.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            return DbSet.Find(email);
        }

        public List<Contact> FindContactsBasedName(string name)
        {
            return DbSet.Where(c => c.FirstName.Contains(name) || c.LastName.Contains(name)).ToList();
        }

        public List<Contact> FindContactsByActivity(Activity activity)
        {
            return
                DbSet.Where(
                    c =>
                        c.Activities.Any(
                            a =>
                                a.Subject == activity.Subject
                                && a.Date == activity.Date
                                && a.Partner.PartnerName == activity.Partner.PartnerName)).ToList();
        }

        public static bool ContactHasCASLActivity(string safeEmail)
        {
            ContactRepository contactRepository = new ContactRepository();
            ActivityRepository activityRepository = new ActivityRepository();
            CampusUnitRepository campusUnitRepository = new CampusUnitRepository();

            bool hasCASLActivity = false;
            var email = safeEmail.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
       
            Contact contact = contactRepository.DbSet.Find(email);
            contact.Activities = activityRepository.FindActivitiesFromContactEmail(email);

            if (contact.Activities != null)
            {
                foreach (var activity in contact.Activities)   
                {
                    activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivityNonTruncateTime(activity);
                    if (activity.CampusUnit != null && activity.CampusUnit.Name == "AS-L")
                    {
                        hasCASLActivity = true;
                        break;
                    }
                }
            }
            return hasCASLActivity;
        }

        public List<string> ReturnAllContactsEmails()
        {
            return DbSet.Select(c => c.Email).OrderBy(x => x).ToList();
        }

        public List<Contact> FindContactsBasedOnNameEmailAndPartnerName(string name, string email, string partnerName)
        {
            var contacts = DbSet.Where(c => c.FirstName.Contains(name) || c.LastName.Contains(name));
            contacts = contacts.Where(c => c.Email.Contains(email));
            contacts = contacts.Where(c => c.Partner.PartnerName.Contains(partnerName));
            return contacts.ToList();
        }
    }
}
