﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;

namespace CPTS.Repository.Context
{
    public class TagRepository : Repository<Tag>
    {
        public List<Tag> FindTagsFromPartnerName(string partnerName)
        {
            return DbSet.Where(t => t.Partners.Any(p => p.PartnerName == partnerName)).ToList();
        }

        public List<string> GetAllTagNames()
        {
            return DbSet.Select(t => t.TagName).ToList();
        }

        public Tag GetTagByName(string tagName)
        {
            return DbSet.Find(tagName);              
        }
    }
}
