﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;
using CPTS.Repository.Helpers;

namespace CPTS.Repository.Context
{
    public class ActivityRepository : Repository<Activity>
    {
        public List<Activity> FindActivitiesFromContactEmail(string email)
        {
            ContactRepository contactRepository = new ContactRepository();
            PartnerRepository partnerRepository = new PartnerRepository();
            CampusUnitRepository campusUnitRepository = new CampusUnitRepository();

            Contact contact = contactRepository.GetSingleContactByEmail(email);
            List<Activity> activities = DbSet.Where(a => a.Contacts.Any(e => e.Email == email)).ToList();
            foreach(var activity in activities)
            {
                activity.Partner = partnerRepository.GetPartnerByActivity(activity);
                activity.CampusUnit = campusUnitRepository.GetCampusUnitFromActivity(activity);
            }

            return activities;
        }

        public Activity FindSingleActivityFromSubject(string safeSubject)
        {
            string subject = CreateSubjectFromSafeSubject(safeSubject);
            return DbSet.FirstOrDefault(a => a.Subject == subject);
        }

        public String FindNotesBySubject(string safeSubject)
        {
            string subject = CreateSubjectFromSafeSubject(safeSubject);
            var tempAct = DbSet.First(a => a.Subject == subject);
            var notes = tempAct.Notes;
            return notes;
        }

        public DateTime GetDateByActivitySubject(string safeSubject)
        {
            string subject = CreateSubjectFromSafeSubject(safeSubject);
            var tempAct = DbSet.First(a => a.Subject == subject);
            var date = tempAct.Date;
            return date;
        }

        public List<Activity> GetAllActivitiesForAPartner(string partnerName)
        {
            return DbSet.Where(a => a.Partner.PartnerName == partnerName).ToList();
        }

        public Activity FindActivityUsingSubjectContactDateAndPartner(string safeSubject, DateTime date, string partnerName = "", string email = "")
        {
            string subject = CreateSubjectFromSafeSubject(safeSubject);
            var activities = new List<Activity>();
            if (email != "")
            {
                activities = DbSet.Where(a => a.Contacts.Any(c => c.Email == email)).ToList();
            }
            else if (partnerName != "")
            {
                activities = DbSet.Where(a => a.Partner.PartnerName == partnerName).ToList();
            }
            activities = activities.Where(a => a.Subject == subject).ToList();
            activities = activities.Where(a => a.Date == date).ToList();
            return activities.FirstOrDefault(); // if more than one Activity has made it this far there is a problem
        }

        public Activity GetActivityFromSubjectAndDate(string subject, DateTime date)
        {
            return DbSet.FirstOrDefault(a => a.Subject == subject && a.Date == date);
        }

        public string CreateSubjectFromSafeSubject(string safeSubject)
        {
            string subject = safeSubject.Replace(ConstantsChars.ReplacementCharForForwardSlash, '/');
            subject = subject.Replace(ConstantsChars.ReplacementCharForColon, ':');
            subject = subject.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            return subject;
        }

        public List<Activity> FindActivitiesBasedOnSubjectAndDate(string subject, string searchDateFrom, string searchDateTo)
        {
            List<Activity> activities = new List<Activity>();
            List<Activity> dateActivities = new List<Activity>();

            if (subject != null && subject != "") activities = DbSet.Where(a => a.Subject.Contains(subject)).ToList();
            else if (subject != null) activities = DbSet.Select(a => a).ToList();

            if (searchDateFrom != null && searchDateFrom != "" && searchDateTo != null && searchDateTo != "")
            {
                try
                {
                    DateTime dateTimeFrom = DateTime.Parse(searchDateFrom); // parsing the users entered dates so that they can use almost any date format
                    DateTime dateTimeTo = DateTime.Parse(searchDateTo);
                    dateTimeTo = dateTimeTo.AddDays(1);

                    foreach (var activity in activities)
                    {
                        if (DateTime.Compare(dateTimeFrom, activity.Date) <= 0 && DateTime.Compare(activity.Date, dateTimeTo) < 0) dateActivities.Add(activity);
                    }
                    activities = dateActivities;
                }
                catch (FormatException)
                {
                    activities.Clear();
                }
            }
            else if (searchDateFrom != null && searchDateFrom != "")
            {
                try
                {
                    DateTime dateTimeFrom = DateTime.Parse(searchDateFrom);
                    string format = "MM/dd/yyyy";
                    string searchDate = dateTimeFrom.ToString(format);

                    foreach (var activity in activities)
                    {                       
                        string activityDate = activity.Date.ToString(format);
                        if (activityDate.Contains(searchDate)) dateActivities.Add(activity);
                    }
                    activities = dateActivities;
                }
                catch (FormatException)
                {
                    activities.Clear();
                }
            }

            activities = activities.OrderByDescending(a => a.Date).ToList();

            return activities;
        }       
    }
}
