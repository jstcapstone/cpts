﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;

namespace CPTS.Repository.Context
{
    public class CampusUnitRepository : Repository<CampusUnit>
    {
        public CampusUnit GetCampusUnitFromActivity(Activity activity)
        {
            return
                DbSet.FirstOrDefault(
                    a =>
                        a.Activities.Any(
                            k =>
                                k.Subject == activity.Subject 
                                && DbFunctions.TruncateTime(k.Date) == DbFunctions.TruncateTime(activity.Date) 
                                && k.Partner.PartnerName == activity.Partner.PartnerName));
        }

        public CampusUnit GetCampusUnitFromActivityNonTruncateTime(Activity activity)
        {
            return DbSet.FirstOrDefault(a => a.Activities.Any(k => k.Subject == activity.Subject 
                    && k.Date == activity.Date && k.Partner.PartnerName == activity.Partner.PartnerName));
        }

        public List<string> GetAllCampusUnitsNames()
        {
            return DbSet.Select(c => c.Name).ToList();
        }

        public CampusUnit GetCampusUnitBasedOnName(string name)
        {
            return DbSet.Find(name);
        }

        public CampusUnit GetCampusUnitFromContactEmail(string contactEmail)
        {
            return DbSet.FirstOrDefault(c => c.Activities.Any(a => a.Contacts.Any(t => t.Email == contactEmail)));
        }

        public List<CampusUnit> FindCampusUnitsForContactEmail(string contactEmail)
        {
            return DbSet.Where(c => c.Activities.Any(a => a.Contacts.Any(t => t.Email == contactEmail))).ToList();
        }

        public List<CampusUnit> FindCampusUnitsForPartner(Partner partner)
        {
            return DbSet.Where(c => c.Activities.Any(a => a.Contacts.Any(p => p.Partner.PartnerName == partner.PartnerName))).ToList();
        }

        public CampusUnit GetCampusUnitForCampusUnitData(CampusUnitData data)
        {
            return
                DbSet.FirstOrDefault(
                    c =>
                        c.CampusUnitDataRecords.Any(
                            u =>
                                u.UWSProgram == data.UWSProgram && u.DepartmentEvent == data.DepartmentEvent &&
                                u.UWSStaff == data.UWSStaff && u.Semester == data.Semester && u.AssignedContact.Email == data.AssignedContact.Email));
        }
    }
}
