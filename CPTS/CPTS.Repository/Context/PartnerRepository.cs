﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;
using System.Data.Entity;
using CPTS.Repository.Helpers;

namespace CPTS.Repository.Context
{
    public class PartnerRepository : Repository<Partner>
    {
        public List<Partner> GetPartnersByName(string name)
        {
            return DbSet.Where(x => x.PartnerName.Contains(name)).ToList();
        }

        public List<Partner> GetByTag(string tag)
        {
            return DbSet.Where(x => x.SearchTags.Any(p => p.TagName.Contains(tag))).ToList();
        }

        public Partner GetSinglePartnerByName(string urlString)
        {
            var name = urlString.Replace(ConstantsChars.ReplacementCharForPeriod, '.');
            name = name.Replace(ConstantsChars.ReplacementCharForColon, ':');
            name = name.Replace(ConstantsChars.ReplacementCharForAnd, '&');
            return DbSet.Find(name);
        }

        public Partner GetPartnerByContactEmail(string email)
        {
            return DbSet.FirstOrDefault(p => p.Contacts.Any(c => c.Email == email));
        }

        public List<Partner> GetByCampusUnit(string campusUnit)
        {
            return DbSet.Where(p => p.Contacts.Any(c => c.Activities.Any(a => a.CampusUnit.Name == campusUnit))).ToList();
        }

        public Partner GetPartnerByActivity(Activity activity)
        {
            return
                DbSet.FirstOrDefault(
                    p =>
                        p.Activities.Any(
                            a =>
                                a.Subject == activity.Subject
                                && a.Date == activity.Date));
        }
    }
}
