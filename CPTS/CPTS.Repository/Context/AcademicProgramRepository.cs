﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;

namespace CPTS.Repository.Context
{
    public class AcademicProgramRepository : Repository<AcademicProgram>
    {
        public List<AcademicProgram> GetByName(string name)
        {
            return DbSet.Where(x => x.ProgramName.Contains(name)).ToList();
        }


    }
}
