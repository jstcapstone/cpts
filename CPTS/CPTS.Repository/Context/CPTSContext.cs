﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;

namespace CPTS.Repository.Context
{
    public class CPTSContext : DbContext
    {
        public DbSet<Partner> Partners { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<CampusUnit> CampusUnits { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<CampusUnitData> CampusUnitDataRecords { get; set; }
    }
}
