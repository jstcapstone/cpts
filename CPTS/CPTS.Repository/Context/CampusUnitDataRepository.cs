﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPTS.Repository.Models;

namespace CPTS.Repository.Context
{
    public class CampusUnitDataRepository : Repository<CampusUnitData>
    {
        public List<CampusUnitData> FindCampusUnitDataFromContactEmail(string email)
        {
            ContactRepository contactRepository = new ContactRepository();
            Contact contact = contactRepository.GetSingleContactByEmail(email);

            List<CampusUnitData> CampusUnitData = DbSet.Where(a => a.AssignedContact.Email == email).ToList();

            return CampusUnitData;
        }

        public CampusUnitData GetSingleCampusUnitDataRecord(string uwsProgram, string uwsDepartmentEvent, string uwsStaff, string semester, string email)
        {
            return
                DbSet.FirstOrDefault(
                    a =>
                        a.UWSProgram == uwsProgram && a.UWSStaff == uwsStaff && a.Semester == semester &&
                        a.DepartmentEvent == uwsDepartmentEvent && a.AssignedContact.Email == email);
        }  
    }
}
