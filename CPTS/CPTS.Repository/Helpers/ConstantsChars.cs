﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPTS.Repository.Helpers
{
    public static class ConstantsChars
    {
        public const char ReplacementCharForAnd = (char) 246;
        public const char CharForStringSeperator = (char) 247;

        public const char ReplacementCharForPeriod = (char) 248;

        public const char ReplacementCharForColon = (char) 249;

        public const char ReplacementCharForForwardSlash = (char) 250;
    }
}
